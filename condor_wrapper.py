#!/bin/python3
import argparse
import os
import os.path as osp
import sys
import time
from subprocess import call, check_output


def strftime(t=None):
    """Get string of current time"""
    return time.strftime("%Y%m%d-%H%M%S", time.localtime(t or time.time()))


def get_parser():

    parser = argparse.ArgumentParser(
        description="""A python wrapper for condor_submit.
To use it user needs to provide the path to an executable, and pass all argument combinations
to that excutable from stdin, one line per argument combination.
"""
    )

    parser.add_argument("exec", metavar="executable", type=str)
    parser.add_argument("--name", type=str, default="", help="default: None")
    parser.add_argument("--cpus", type=int, default=1)
    parser.add_argument("--gpus", type=int, default=0)
    parser.add_argument("--mem", type=float, default=1, help="default: 1 (GB)")
    parser.add_argument(
        "--disk", type=float, default=1, help="default: 1 (GB)"
    )

    parser.add_argument(
        "--universe", type=str, default="vanilla", help="default: vanilla"
    )
    parser.add_argument(
        "--requirements", type=str, default="", help="default: None"
    )
    parser.add_argument(
        "--notify_mode", type=str, default="ERROR", help="default: ERROR"
    )
    parser.add_argument("--wait", type=int, default=1, help="default: 1")
    parser.add_argument(
        "--getenv",
        action="store_true",
        help="whether to copy the environment variables.",
    )
    parser.add_argument(
        "--batch",
        action="store_true",
        help="batch all job into a single DAG job",
    )

    return parser


def submit_condor_job(args):
    now = strftime()
    os.makedirs("condor/{}".format(now))

    exec_path = check_output(["which", args.exec])
    args.exec = osp.expanduser(exec_path.decode("utf")).strip()

    arg_file = "condor/{}/_arguments".format(now)
    with open(arg_file, "w") as fout:
        for line in sys.stdin:
            fout.writelines(line)
            print(args.exec + " " + line.strip())

    submit_file = f"""
JobBatchName    = {args.name}
universe        = {args.universe}
request_cpus    = {args.cpus}
request_memory  = {args.mem}GB
request_gpus    = {args.gpus}
request_disk    = {args.disk}GB
requirements    = {args.requirements}
should_transfer_files = YES
when_to_transfer_output = ON_EXIT_OR_EVICT

executable      = {args.exec}
output          = condor/{now}/$(Cluster).$(Process).out
log             = condor/{now}/$(Cluster).$(Process).log
error           = condor/{now}/$(Cluster).$(Process).err

getenv              = {args.getenv}
notification        = {args.notify_mode}
notify_user         = zhangwei@cs.wisc.edu
max_retries        = 0
success_exit_code  = 0

arguments = $(args)
queue args from {arg_file}
"""

    condor_file = "condor/{}/_submit_file".format(now)
    with open(condor_file, "w") as fout:
        fout.writelines(submit_file)

    if not args.batch:
        call("condor_submit " + condor_file, shell=True)
    else:
        dag_file = "condor/{}/_dag".format(now)
        with open(dag_file, "w") as fout:
            fout.writelines("JOB A {}".format(condor_file))
        # FIXME: somehow the notification still not working
        call(
            "condor_submit_dag -notification always "
            "-dont_suppress_notification " + dag_file,
            shell=True,
        )

    time.sleep(args.wait)


if __name__ == "__main__":

    args = get_parser().parse_args()

    submit_condor_job(args)
