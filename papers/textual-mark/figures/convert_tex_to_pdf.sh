#!/bin/bash

for file in *.tex; do
    #echo $file
    if [ $file != tikzlibrarybayesnet.code.tex ]; then
        pdflatex $file;
    fi
done

rm *.aux *.log