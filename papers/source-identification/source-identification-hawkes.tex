%% LyX 2.2.0 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[12pt,letterpaper,preprint]{article}
\usepackage[latin9]{inputenc}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{esint}
\usepackage[authoryear]{natbib}
\usepackage[unicode=true,
 bookmarks=false,
 breaklinks=false,pdfborder={0 0 1},backref=section,colorlinks=false]
 {hyperref}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
\pdfpageheight\paperheight
\pdfpagewidth\paperwidth


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage{amsthm}
\usepackage{bm}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{algpseudocode}
\usepackage{algorithm}



\newcommand{\footnoteref}[1]{\protected@xdef\@thefnmark{\ref{#1}}\@footnotemark}


\let\cite\citep


% user-defines

\newcommand{\bth}{\bm{\theta}}
\renewcommand{\L}{\mathcal{L}}
\DeclareMathOperator*{\KL}{\text{KL}}
\DeclareMathOperator*{\E}{\mathbb{E}}
\DeclareMathOperator*{\conv}{\text{conv}}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}

\makeatother

\begin{document}
\tableofcontents{}

\global\long\def\H{\mathcal{H}}


\part{Source identification Hawkes process}

\section{Model}

Supposing we have two point processes, namely Twitter $\{t_{i}^{(T)}\}$
and News $\{t_{i}^{(N)}\}$. Their corresponding counting process
is denoted by $N^{(T)}(t)$ and $N^{(N)}(t)$ respectively. And we
assume both processed are observed from $t=0$ to $t=t_{\max}$.

Restricting to our usage scenario, we assume that Twitter events $\{t_{i}^{(T)}\}$
is produced by its base-line intensity, self excitation and the excitation
from $\{t_{i}^{(N)}\}$. Schematically, it can be represented as

\begin{equation}
\{t_{i}^{(T)}\}\leftarrow\mu(t)+\mathcal{K}^{(T\rightarrow T)}(\cdot)+\mathcal{K}^{(N\rightarrow T)}(\cdot),
\end{equation}
where $\mu(t)$ is the baseline intensity, $\mathcal{K}^{(T\rightarrow T)}$
is the self-excitation kernel, and $\mathcal{K}^{(N\rightarrow T)}$
is the excitation kernel from news to tweets.

We can parametrize a kernel by a positive scalar and a normalized
kernel, namely

\begin{equation}
\mathcal{K}^{(T\rightarrow T)}(s,t)=\gamma^{(T\rightarrow T)}K^{(T\rightarrow T)}(s,t),
\end{equation}
which satisfies $\int_{s}^{\infty}K^{(T\rightarrow T)}(s,t)dt=1$
for all $s$. The parametrization works similarly for $\mathcal{K}^{(N\rightarrow T)}(s,t)$.

The generative model for Tweets, conditioned on the News stream, is
described by the conditional rate below.

\begin{align}
 & \frac{dN^{(T)}(t)}{dt}|\{t_{i}^{(T)}:t_{i}^{(T)}<t\},\{t_{i}^{(N)}:t_{i}^{(N)}<t\}\\
 & =\mu(t)+\gamma^{(T\rightarrow T)}\int_{-\infty}^{t}K^{(T\rightarrow T)}(s,t)dN^{(T)}(s)+\gamma^{(N\rightarrow T)}\int_{-\infty}^{t}K^{(N\rightarrow T)}(u,t)dN^{(N)}(u).\nonumber 
\end{align}

The likelihood is given by a Poisson process with the conditional
rate above 
\begin{equation}
\mathcal{L}(\{t_{i}^{(T)}\}|\cdots)=\prod_{i}\frac{dN^{(T)}(t_{i}^{(T)})}{dt}\exp\left(-\int_{0}^{T}\frac{dN^{(T)}(s)}{dt}ds\right).
\end{equation}

%------------------------------------------------

\section{Bayesian Inference}

By introducing a latent indicator variable $z_{i}^{(T)}$ corresponding
to the source generating $t_{i}^{(T)}$, we can perform a two-stage
Bayesian inference, which consists of 
\begin{enumerate}
\item Imputing 
\[
z_{i}^{(T)}=\begin{cases}
0,\quad\text{baseline}\\
1,\quad\text{self excitation}\\
2,\quad\text{excitation from News}
\end{cases}
\]
for each event $t_{i}^{(T)}$. 
\item Conditioned on $\{z_{i}^{(T)}\}$, inferring baseline rate and kernels. 
\end{enumerate}

\subsection{Simplest Gibbs Sampler: Constant Baseline Rate}

By assuming a constant baseline rate

\begin{equation}
\mu(t)=\mu\sim\text{Gamma}(a,b),
\end{equation}
and other conjugate priors for scalar multipliers $\gamma^{(T\rightarrow T)}\sim\text{Gamma}(a^{T\rightarrow T},b^{T\rightarrow T})$
and $\gamma^{(N\rightarrow T)}\sim\text{Gamma}(a^{N\rightarrow T},b^{N\rightarrow T})$,
a simple Gibbs sampler can be easily constructed. Notice that this
parametrization can be extended to $\eta(t)\mu_{0}$ for some fixed
function $\eta(t)\geq0$. 
\begin{enumerate}
\item The conditional probability for immigration indicator is given by
the relative intensities.

\[
p(z_{i}^{(T)}|\cdots)\propto\begin{cases}
\mu,\quad(z_{i}^{(T)}=0)\\
\gamma^{(T\rightarrow T)}\int_{-\infty}^{t_{i}^{(T)}}K^{(T\rightarrow T)}(s,t_{i}^{(T)})dN^{(T)}(s),\quad(z_{i}^{(T)}=1)\\
\gamma^{(N\rightarrow T)}\int_{-\infty}^{t_{i}^{(T)}}K^{(N\rightarrow T)}(s,t_{i}^{(T)})dN^{(N)}(s),\quad(z_{i}^{(T)}=2)
\end{cases}.
\]

We are mostly interested in inferring the latent variables for each
tweet, as they measure the evidence of a tweet being ``organic''.
\item The baseline rate has the full conditional as

\[
p(\mu|\cdots)\propto\text{Gamma}(\mu;a,b)\times\mu^{\sum_{i}\mathbf{1}(z_{i}^{(T)}=0)}\exp(-\mu T)=\text{Gamma}(a+n_{0},b+T),
\]
where $n_{0}=\sum_{i}\mathbf{1}(z_{i}^{(T)}=0)$.
\item $\gamma^{(T\rightarrow T)}$ measures on average how many tweets that
a tweet generates by self-excitation, which has the following full
conditional.

\begin{align*}
p(\gamma^{(T\rightarrow T)}|\cdots) & \propto\text{Gamma}(\gamma^{(T\rightarrow T)};a^{T\rightarrow T},b^{T\rightarrow T})\\
 & \times\prod_{i:z_{i}^{(T)}=1}\gamma^{(T\rightarrow T)}\times\exp\left(-\gamma^{(T\rightarrow T)}\sum_{i=1}^{N^{(T)}(T)}\int_{t_{i}^{(T)}}^{T}K^{(T\rightarrow T)}(t_{i}^{(T)},s)dN^{(T)}(s)\right),
\end{align*}
namely 
\begin{align*}
p(\gamma^{(T\rightarrow T)}|\cdots) & =\text{Gamma}\left(a^{T\rightarrow T}+n_{1},b^{T\rightarrow T}+\sum_{i=1}^{N^{(T)}(T)}\int_{t_{i}^{(T)}}^{T}K^{(T\rightarrow T)}(t_{i}^{(T)},s)dN^{(T)}(s)\right)\\
 & \approx\text{Gamma}\left(a^{T\rightarrow T}+n_{1},b^{T\rightarrow T}+N^{(T)}(T)\right),
\end{align*}
where $n_{1}=\sum_{i}\mathbf{1}(z_{i}^{(T)}=1)$, and $N^{(T)}(T)$
is the total number of tweets observed.
\item Similarly, we have

\[
p(\gamma^{(N\rightarrow T)}|\cdots)\approx\text{Gamma}\left(a^{N\rightarrow T}+n_{2},b^{N\rightarrow T}+N^{(N)}(T)\right),
\]
where $n_{2}=\sum_{i}\mathbf{1}(z_{i}^{(T)}=2)$, and $N^{(N)}(T)$
is the total number of news events observed.
\end{enumerate}

\subsection{Inferring Kernel Parameters}

Consider the case where the normalized kernel 
\[
K^{(T\rightarrow T)}=K_{\phi}^{(T\rightarrow T)}
\]
is parametrized by $\phi$. We can optimize the kernel parameters
by maximizing the marginal likelihood (type-II maximum likelihood).

\section{Discussions}

Now suppose we have a general baseline rate function $\mu(t)$ and
consider its inference. The likelihood terms, using the latent variable
representation, become 
\[
\prod_{i:z_{i}^{(T)}=0}\mu(t_{i}^{(T)})\times\exp\left(-\int_{0}^{T}\mu(t)dt\right).
\]

For modeling a smooth non-negative $\mu(t)$, one can consider a Gaussian
process with exponential transform

\[
u(t)\sim\text{GP}(\mu_{0},\Sigma(\cdot,\cdot)),\quad\mu(t)=\exp(u(t)).
\]

To evaluate the relevant likelihood terms, we must (1) impute the
sample path on certain time points $\{t_{i}^{(T)}:z_{i}^{(T)}=0\}$
and (2) evaluate or approximate the integral of sample path. For (2),
one can consider imputing points on a grid and approximating the integral
by a finite sum.

The updates for both (1) and (2) are non-conjugate and we must resort
to Metropolis-Hastings or slice sampling. 
\end{document}
