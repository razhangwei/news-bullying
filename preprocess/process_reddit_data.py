"""Process the raw reddit data and export as DATA format """
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
import joblib


for data_name in ["0.25K", "1K"]:

    df = pd.read_csv(
        "../data/working/reddit-%s/reddit_politics_comments.csv" % data_name,
        index_col=0,
        parse_dates=["date_time"],
        encoding='ISO-8859-1"',
        error_bad_lines=False,
    )

    df.sort_values("date_time", ascending=True, inplace=True)

    vectorizer = CountVectorizer(
        min_df=1, max_df=0.8, stop_words="english"
    ).fit(df.text_mark)

    temp = vectorizer.transform(df.text_mark)
    idx = temp.sum(axis=1).getA1() != 0
    df = df[idx]
    temp = temp[idx]

    df["bow"] = [temp[i, :] for i in range(temp.shape[0])]

    start_date = df.date_time.min().to_datetime64()

    dimen_code = {}
    for s in df.source:
        if s not in dimen_code:
            dimen_code[s] = len(dimen_code)

    events = []
    for i in range(len(df)):
        t = (df.iloc[i].date_time - start_date) / pd.to_timedelta("1s")
        s = dimen_code[df.iloc[i].source]
        x = df.iloc[i].bow
        events.append((t, s, x))

    export_data = {
        "dataframe": df,
        "events": events,
        "start_date": start_date,
        "dimen_code": dimen_code,
        "vocabulary": vectorizer.vocabulary_,
    }

    output_path = "../data/working/reddit-%s/DATA.pkl" % data_name

    joblib.dump(export_data, output_path)
