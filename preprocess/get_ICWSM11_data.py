"""Extract ICWSM dataset."""
import pandas as pd
import joblib
import argparse


INPUT_FOLDER = "../data/working/ICWSM11-bully"
OUTPUT_FOLDER = "../data/working/ICWSM11-bully"
VALID_LENGTHS = ["1day", "1week"]


def main(args):

    df_news = pd.read_pickle("%s/MAINSTREAM_NEWS_bullying.pkl" % INPUT_FOLDER)
    df_social = pd.read_pickle("%s/SOCIAL_MEDIA_bullying.pkl" % INPUT_FOLDER)
    vectorizer = joblib.load("%s/vectorizer.pkl" % INPUT_FOLDER)
    # prepare the data
    start_date = min(
        df_social.date.iloc[0], df_news.date.iloc[0]
    ).to_datetime64()

    for l in args.L:
        if l == "1day":
            end_date = start_date + pd.to_timedelta("1day")
        elif l == "1week":
            end_date = start_date + pd.to_timedelta("7day")
        print("Processing %s data ..." % l)

        events = []
        for s, df in enumerate([df_social, df_news]):
            t = df[df.date < end_date]
            for i in range(len(t)):
                date, bow = t.iloc[i][["date", "bow"]]
                date = (date - start_date) / pd.to_timedelta("1s")
                events.append((date, s, bow))

        events = sorted(events, key=lambda x: x[0])

        export_data = {
            "dataframe": None,
            "events": events,
            "start_date": start_date,
            "dimen_code": {"tweet": 0, "news": 1},
            "vocabulary": vectorizer.vocabulary_,
        }

        output_path = "%s-%s/DATA.pkl" % (OUTPUT_FOLDER, l)
        # initialize_directories([output_path[:output_path.rindex("/")]])
        joblib.dump(export_data, output_path, compress=3)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Extract ICWSM data")
    # add variable arguments
    parser.add_argument(
        "L", type=str, nargs="+", help="Length. Can only be in [1day, 1week]"
    )
    # add actions
    args = parser.parse_args()

    # vanity check for input arguments
    for l in args.L:
        assert l in VALID_LENGTHS, "%s is not in %s" % (l, VALID_LENGTHS)

    main(args)
