from __future__ import print_function
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), "../lib"))

import numpy as np
import joblib

from model.base_intensity import ConstantBaseIntensity
from model.impact_function import ConstantImpactFunction
from model.kernels import ExponentialKernel
from model.mark_density import TextualMarkDensity
from model.source_identify_model import SourceIdentifyModel


def parse_args():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("n_dimensions", type=int)
    parser.add_argument("n_events", type=int)
    parser.add_argument("--kernel_scale", type=int, default=10)
    parser.add_argument("--vocab_size", type=int, default=5000)
    parser.add_argument("--mix_weight", type=float, default=0.1)
    parser.add_argument("--base_rate", type=float, default=0.05)
    parser.add_argument("--self_rate", type=float, default=0.05)
    parser.add_argument("--mutual_rate", type=float, default=0.15)
    parser.add_argument("--rand_seed", type=int, default=0)

    return parser.parse_args()


def generate_data(args):
    np.random.seed(args.rand_seed)
    n = args.n_dimensions
    A = np.ones((n, n)) * args.mutual_rate
    np.fill_diagonal(A, args.self_rate)

    mark_density_config = {
        "n_features": args.vocab_size,
        "lengths": [10 + 10 * i for i in range(n)],
        "feature_probs": np.random.dirichlet(np.ones(args.vocab_size), n),
        "weight": args.mix_weight,
    }
    config = {
        "n_dimensions": n,
        "base_intensities": [ConstantBaseIntensity()] * n,
        "base_intensity_weights": [args.base_rate] * n,
        "influential_matrix": A,
        "mark_density": TextualMarkDensity(**mark_density_config),
        "kernels": [ExponentialKernel(args.kernel_scale) for i in range(n)],
        "impact_function": ConstantImpactFunction(),
    }

    model = SourceIdentifyModel(**config)
    events, parents = model.simulate(n_max=args.n_events)
    roots = model.eval_roots(indices=[e[1] for e in events], parents=parents)
    # export
    export_folder = os.path.join(
        os.path.dirname(__file__),
        "../data/working/",
        "syn{}-{}K".format(n, args.n_events / 1000),
    )
    if not os.path.exists(export_folder):
        os.makedirs(export_folder)

    export_data = {
        "events": events,
        "roots": roots,
        "dimen_code": [str(i) for i in range(n)],
        "vocabulary": [str(i) for i in range(args.vocab_size)],
    }

    joblib.dump(export_data, export_folder + "/DATA.pkl")
    with open(export_folder + "/README", "w") as fout:
        fout.write(str(args))


if __name__ == "__main__":
    args = parse_args()
    generate_data(args)
    # print(args)
