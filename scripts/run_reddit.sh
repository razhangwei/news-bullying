#!/bin/bash

export CUDA_DEVICE_ORDER="PCI_BUS_ID"

WS=`pwd`
# LOCAL_RUN="while read line; do echo $line; done"
LOCAL_RUN="xargs -L1 python"
CONDOR_RUN="python condor_wrapper.py python --getenv --requirements (Machine!=\"mastodon-2.biostat.wisc.edu\")"
SSH_RUN="python ssh_wrapper.py python --gpus 1 -N 2 --sleep 5 --email"

# Helper functions
function check_condor_completion() {
    batch_name=${1:-undefined}
    refresh_freq=${2:-1m}

    # presleep
    sleep 5s
    while condor_q -af JobBatchName| grep -q $batch_name; do
        condor_q
        echo condor jobs haven\'t finished. waiting...
        sleep $refresh_freq
    done
}

function send_email(){
    text=$1
    subject=${2:-'Experiment Notification'}
    recepient=${3:-'zhangwei@cs.wisc.edu'}

    mail -s $subject $recepient <<< $text
}

# send_email "Experiment Started."
python json_expd.py configs/reddit.json --add_prefix $WS/tasks/reddit_analysis.py | $CONDOR_RUN --cpus 1
check_condor_completion
# send_email "Experiment Finished."
