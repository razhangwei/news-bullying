#!/bin/bash

PARA_RUN="lib/model/utils/parallel_run.py"
TASK_DIR="~/workspace/hawkes-rooted-probability/tasks"

DATASET="12-angry-men"
KERNEL_SCALES="1,3,5,7,10"

DEFAULT_FLAGS="--pw hugh0709 --use_python2"
TUNE_FLAGS="--dataset $DATASET --kernel_scale $KERNEL_SCALES"

python $PARA_RUN "python ${TASK_DIR}/tune.py --force" $DEFAULT_FLAGS $TUNE_FLAGS
python $PARA_RUN "python ${TASK_DIR}/train.py --force" $DEFAULT_FLAGS $TUNE_FLAGS

#python $PARA_RUN "python ${TASK_DIR}/tune.py" $DEFAULT_FLAGS $TUNE_FLAGS
