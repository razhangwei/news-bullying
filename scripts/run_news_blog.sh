export PYTHONPATH=`pwd`/..
cd ../tasks

DATASET=news-blog-top10
BASE_PERIODS=604800
KERNEL_SCALE="3600 7200"

python experiments_real_dataset.py --dataset  $DATASET --base_periods $BASE_PERIODS \
    --kernel_scale $KERNEL_SCALE --email
