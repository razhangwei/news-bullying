#!/bin/python3
import argparse
import json
import random
from collections import OrderedDict
from itertools import product


def get_parser():
    parser = argparse.ArgumentParser(
        description="""Expand a param_grid represented in JSON to a list of \
parameter options. The JSON content is read from stdin.
"""
    )
    parser.add_argument("json_file", type=str, help="Path to json file.")
    parser.add_argument(
        "--add_prefix", type=str, default="", help="Default: None"
    )
    # parser.add_argument("--sort_param", action="store_true")
    parser.add_argument("--rand_seed", type=int, default=0)

    return parser


def ParameterGrid(param_grid):
    from collections import Mapping

    if isinstance(param_grid, Mapping):
        # wrap dictionary in a singleton list to support either dict
        # or list of dicts
        param_grid = [param_grid]

    params_list = []

    for p in param_grid:
        if "_max_comb" in p:
            max_comb = p["_max_comb"]
            rand_seed = p["_rand_seed"]
            del p["_max_comb"]
            del p["_rand_seed"]
        else:
            max_comb = 0

        items = p.items()
        if not items:
            params_list.append({})
        else:
            keys, values = zip(*items)
            values = (v if isinstance(v, list) else [v] for v in values)
            v_arr = list(product(*values))
            if max_comb:
                random.seed(rand_seed)
                v_arr = random.sample(v_arr, min(max_comb, len(v_arr)))

            for v in v_arr:
                params = OrderedDict(zip(keys, v))
                params_list.append(params)

    return params_list


args = get_parser().parse_args()

with open(args.json_file, "r") as fin:
    param_grid = json.load(fin, object_pairs_hook=OrderedDict)

params_list = ParameterGrid(param_grid)

for param in params_list:
    cmd = args.add_prefix
    for param_name, param_value in param.items():

        if isinstance(param_value, str):
            param_value_str = param_value
            if " " in param_value_str:
                raise ValueError(
                    "string parameter can't contain whitespace. "
                    "Found: '{}'".format(
                        param_value_str
                    )
                )
        else:
            param_value_str = json.dumps(param_value)

        # escape double quotes
        param_value_str = param_value_str.replace('"', '\\"')

        # surround it with single quote if containing whitespace
        if " " in param_value_str:
            param_value_str = param_value_str.replace(" ", "")
            # param_value_str = '{}'.format(param_value_str)

        if param_value_str != "null":
            cmd += " --{} {}".format(param_name, param_value_str)
        else:
            cmd += " --{}".format(param_name)
    print(cmd)
