from unittest import TestCase
import numpy as np

from base_intensity import SineBaseIntensity


class TestSineBaseIntensity(TestCase):
    @classmethod
    def setUpClass(cls):
        n = 2
        periods = np.array([3600 * 24, 3600 * 24 * 7])
        amplitudes = np.array([10, 1, 1])
        shifts = np.zeros(n)
        cls.base_intensity = SineBaseIntensity(
            n_components=1,
            amplitudes=amplitudes,
            periods=periods,
            shifts=shifts,
        )

    def test_eval(self):
        t = np.arange(1000) * 3600
        y = TestSineBaseIntensity.base_intensity.eval(t)
        self.assertEqual(y.ndim, 1)
        self.assertEqual(len(t), len(y))

    def test_eval_integral(self):
        t = np.arange(1000) * 3600
        integral = TestSineBaseIntensity.base_intensity.eval_integral(t)
        self.assertEqual(integral.ndim, 1)
        self.assertEqual(len(t), len(integral))
        self.assertEqual(integral[0], 0)

    def test_eval_upperbound(self):
        t = np.arange(1000) * 3600
        y = TestSineBaseIntensity.base_intensity.eval_upperbound(t)
        self.assertEqual(y.ndim, 1)
        self.assertEqual(len(t), len(y))

    def test_init_params(self):
        t = []
        for i in range(24 * 7):
            tt = (0.5 + i) * 3600
            t += [tt] * int(TestSineBaseIntensity.base_intensity.eval(tt))

        TestSineBaseIntensity.base_intensity.init_params(
            [(tt, None, None) for tt in t]
        )
