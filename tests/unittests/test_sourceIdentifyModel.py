from unittest import TestCase
import numpy as np

from model.base_intensity import SineBaseIntensity
from model.impact_function import ConstantImpactFunction
from model.kernels import ExponentialKernel
from model.mark_density import TextualMarkDensity
from model.source_identify_model import SourceIdentifyModel


class TestSourceIdentifyModel(TestCase):
    @classmethod
    def setUpClass(cls):

        np.random.seed(0)

        n_dimensions = 2
        base_intensity1 = SineBaseIntensity(
            n_components=1, periods=[1000], amplitudes=[1, 0.5]
        )
        base_intensity2 = SineBaseIntensity(
            n_components=1, periods=[1000], amplitudes=[1, 0.5], shifts=[np.pi]
        )
        base_intensities = [base_intensity1, base_intensity2]

        base_intensity_weights = [1, 1]
        influential_matrix = np.array([[0.4, 0.2], [0.2, 0.4]])

        kernels = [ExponentialKernel(10), ExponentialKernel(20)]
        impact_function = ConstantImpactFunction()
        n_features = 50000
        feature_probs = np.random.dirichlet(np.ones(n_features), 2)
        mark_density = TextualMarkDensity(
            n_features,
            lengths=[10, 500],
            feature_probs=feature_probs,
            weight=0.3,
        )

        cls.model = SourceIdentifyModel(
            n_dimensions,
            base_intensities=base_intensities,
            base_intensity_weights=base_intensity_weights,
            influential_matrix=influential_matrix,
            kernels=kernels,
            impact_function=impact_function,
            mark_density=mark_density,
        )

    def test_load_and_save(self):
        TestSourceIdentifyModel.model.save("test_save")
        SourceIdentifyModel.load("test_save")
        import os

        os.remove("test_save")

    # def test_simulate(self):
    #     self.fail()
    #
    # def test_init_params(self):
    #     self.fail()
    #
    # def test_fit(self):
    #     self.fail()
    #
    # def test_eval_rooted_proba(self):
    #     self.fail()
    #
    # def test_eval_log_likelihood(self):
    #     self.fail()
    #
    # def test_eval_intensity_integral(self):
    #     self.fail()
    #
    # def test_eval_conditional_intensity(self):
    #     self.fail()
    #
    # def test_eval_variational_lower_bound(self):
    #     self.fail()
    #
    # def test_cumulative_transform(self):
    #     self.fail()
    #
    # def test_eval_roots(self):
    #     self.fail()
