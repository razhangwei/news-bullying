import sys
import argparse
import os.path as osp
from copy import deepcopy

sys.path.append(osp.join(osp.dirname(__file__), "..", "lib"))

from model.utils.log_utils import init_logging, get_logger
from utils import get_folders, get_hparam_str, load_dataset, get_model_config

import joblib

# configuaration
TASK_NAME = "TrainOnFullData"
INPUT_FOLDER, OUTPUT_FOLDER, LOGGING_FOLDER = get_folders(TASK_NAME)


def build_args():
    parser = argparse.ArgumentParser(
        description="Running experiments on real dataset."
    )
    # add variable arguments
    parser.add_argument(
        "--dataset",
        type=str,
        help="Dataset. Can only be in [ICWSM11-bully-1week, "
        "news_blog_top10, reddit-0.25K, reddit-1K]",
    )

    parser.add_argument(
        "--base_intensity_shape",
        type=str,
        default="constant",
        help="The shape for base intensity function. "
        "Can only be [constant, sine]. Default=constant",
    )
    parser.add_argument(
        "--base_periods",
        type=int,
        nargs="+",
        default=None,
        help="Periods for base sine base intensity. " "Default is None.",
    )
    parser.add_argument(
        "--base_constant",
        type=float,
        default=1.0,
        help="Constant value for ConstantBaseIntensity.",
    )

    parser.add_argument(
        "--kernel_scale",
        type=int,
        default=3600,
        help="Scale paramter shared by all exponential kernels."
        "Default is 3600",
    )

    # add actions
    parser.add_argument(
        "--email", action="store_true", help="Send email to notify. "
    )
    parser.add_argument(
        "--force",
        action="store_true",
        help="Force to run the experiments no matter "
        "if results have already been cached.",
    )

    args = parser.parse_args()

    # sanity check for inputs
    assert args.dataset is not None
    assert args.base_intensity_shape in ["constant", "sine"]
    assert not (
        args.base_intensity_shape == "sine" and args.base_periods is None
    )

    return args


def fit_model(data, args, logger):

    from model.source_identify_model import SourceIdentifyModel

    model_path = "{}/{}/model_{}.pkl".format(
        OUTPUT_FOLDER, args.dataset, get_hparam_str(args)
    )

    if not args.force and osp.exists(model_path):
        logger.info("Found model. ")
        model = SourceIdentifyModel.load(model_path)
    else:
        logger.info("Training model for args: {}".format(args))
        model = SourceIdentifyModel(**get_model_config(data, args))
        model.fit(data["events"], verbose=True)
        model.save(model_path)
        logger.info("Finished. ")

    return model


def main(args):
    """
    """
    init_logging(osp.join(LOGGING_FOLDER, args.dataset), level="INFO")
    logger = get_logger(__name__)
    logger.info(" ".join(sys.argv))

    # load dataset
    data = load_dataset(args.dataset, INPUT_FOLDER)
    model_path = "{}/{}/model_{}.pkl".format(
        OUTPUT_FOLDER, args.dataset, get_hparam_str(args)
    )

    try:
        model = fit_model(data, args, logger)

        # computing the rooted probability
        rp_path = model_path.replace("model", "rp")
        if not args.force and osp.exists(rp_path):
            logger.info("Found the computed rooted probabilities. ")
        else:
            logger.info(
                "Computing rooted probabilities for args: {}".format(args)
            )
            rp = model.eval_rooted_proba(data["events"])
            logger.info("Finished.")
            joblib.dump(rp, rp_path)

        # export summary
        summary_path = model_path.replace("model", "summary")
        if args.force or not osp.exists(summary_path):
            logger.info("Exporting results for args: {}".format(args))
            rp = joblib.load(rp_path)

            # sanity check
            df = data["dataframe"]
            assert len(data["events"]) == len(df)

            summary = deepcopy(data)
            df["root_proba"] = [rp[i] for i in range(rp.shape[0])]
            summary["dataframe"] = df
            summary["influential_matrix"] = model.influential_matrix
            summary["rp"] = rp

            joblib.dump(summary, summary_path)

    except Exception:
        logger.error("Error occured.", exc_info=True)


if __name__ == "__main__":

    args = build_args()
    main(args)
