# %%
import joblib
import sys
import os
import argparse
import os.path as osp
from collections import namedtuple

import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer

os.chdir(os.path.join(osp.dirname(__file__), ".."))
sys.path.append(".")

from lib.model.base_intensity import ConstantBaseIntensity
from lib.model.kernels import ExponentialKernel
from lib.model.mark_density import TextualMarkDensity
from lib.model.source_identify_model import SourceIdentifyModel
from lib.utils.misc import makedirs, export_to_csv

Result = namedtuple(
    "Result", ["model", "kernel_scale", "c_prop", "acc", "total_log_prob"]
)


def get_parser():
    parser = argparse.ArgumentParser(description="Experiments on Reddit data")
    parser.add_argument(
        "--input_file",
        type=str,
        default="data/input/reddit_political_comments_10_30.csv",
        help="default: data/input/reddit_political_comments_10_30.csv",
    )
    # add variable arguments
    parser.add_argument(
        "--output_path",
        type=str,
        default="data/output/Reddit",
        help="default: data/output/Reddit",
    )

    parser.add_argument(
        "--model", type=str, default="RP_FIT", help="default: RP_FIT"
    )
    parser.add_argument("--rho", type=float, default=1.0, help="default: 1.")
    parser.add_argument(
        "--c_prop", type=float, default=0.5, help="default: 0.5"
    )
    parser.add_argument(
        "--kernel_scale", type=float, default=32, help="default: 32"
    )

    # vocabulary config
    parser.add_argument("--min_df", type=float, default=1, help="default: 1")
    parser.add_argument(
        "--max_df", type=float, default=0.8, help="default: 0.8"
    )

    parser.add_argument("--rand_seed", type=int, default=0, help="default: 0")

    return parser


args = get_parser().parse_args()
assert args.model in ["RP_FIT", "RP_MARK", "RP_TIME"]

makedirs([args.output_path])

data_path = osp.join(args.output_path, "reddit_DATA.pkl")

try:
    data = joblib.load(data_path)

except Exception:
    print("Processing dataset...")
    df = pd.read_csv(
        args.input_file,
        parse_dates=["date_time"],
        encoding='ISO-8859-1"',
        error_bad_lines=False,
    )

    df.sort_values("date_time", ascending=True, inplace=True)

    vectorizer = CountVectorizer(
        min_df=args.min_df, max_df=args.max_df, stop_words="english"
    ).fit(df.text_mark)

    temp = vectorizer.transform(df.text_mark)
    idx = temp.sum(axis=1).getA1() != 0
    df = df[idx]
    temp = temp[idx]

    df["bow"] = [temp[i, :] for i in range(temp.shape[0])]
    start_date = df.date_time.min().to_datetime64()

    dimen_code = {}
    for s in df.source:
        if s not in dimen_code:
            dimen_code[s] = len(dimen_code)

    events = []
    for i in range(len(df)):
        t = (df.iloc[i].date_time - start_date) / pd.to_timedelta("1s")
        s = dimen_code[df.iloc[i].source]
        x = df.iloc[i].bow
        events.append((t, s, x))

    data = {
        "dataframe": df,
        "events": events,
        "start_date": start_date,
        "dimen_code": dimen_code,
        "vocabulary": vectorizer.vocabulary_,
    }

    joblib.dump(data, data_path)

n_dimensions = len(data["dimen_code"])
n_features = len(data["vocabulary"])

config = {}
config["base_intensities"] = [
    ConstantBaseIntensity(args.rho) for _ in range(n_dimensions)
]
config["kernels"] = [
    ExponentialKernel(scale=args.kernel_scale) for _ in range(n_dimensions)
]
config["mark_density"] = TextualMarkDensity(
    n_features, lengths=np.zeros(n_dimensions), feature_probs=None, weight=None
)

model = SourceIdentifyModel(n_dimensions, **config)

model.fit(data["events"], c_proportion=args.c_prop, verbose=True)

# Collect baseline comment rates and source source influences and compute
# root probabilities:
# baseline_comment_rates = model.base_intensity_weights
# source_source_influences = model.influential_matrix

if args.model == "RP_FIT":
    root_probs = model.eval_rooted_proba(data["events"])
elif args.model == "RP_MARK":
    root_probs = model.eval_rooted_proba(data["events"], include_time=False)
elif args.model == "RP_TIME":
    root_probs = model.eval_rooted_proba(data["events"], include_mark=False)

# Save the results:
# Collect source names:
# source_dict = dict((v, k) for k, v in data['dimen_code'].iteritems())
# source_names = [source_dict[i] for i in range(len(source_dict))]

# Create data frames of root probs, influence matrix, and baseline comment
# rates:
# df_baseline_rates = pd.DataFrame(baseline_comment_rates)
# df_source_influence = pd.DataFrame(source_source_influences,
#                                    columns=source_names)
# df_root_prob = pd.DataFrame(root_probs, columns=source_names)

# # Write to .csv:
# hparam_str = f"kernel_scale={kernel_scale},c_prop={c_proportion}"
# df_baseline_rates.to_csv(f"baseline_rates-{hparam_str}.csv")
# df_source_influence.to_csv(f"source_influence-{hparam_str}.csv")
# df_root_prob.to_csv(f"root_prob-{hparam_str}.csv")

# Evaluation
root_source = data["dataframe"]["root_cause"]

# Evaluate and return the accuracy of the root probs on true root source:
true_root_sources = np.array(
    [data["dimen_code"][source] for source in root_source]
)
pred_root_sources = np.argmax(root_probs, 1)

acc = (pred_root_sources == true_root_sources).mean()
total_log_prob = np.log(root_probs)[
    range(len(true_root_sources)), true_root_sources
].sum()
# compute the log predict probability

df_res = pd.DataFrame(
    [Result(args.model, args.kernel_scale, args.c_prop, acc, total_log_prob)],
    columns=Result._fields,
)

print(df_res)
export_to_csv(df_res, osp.join(args.output_path, "results.csv"))
