import os.path as osp


def get_folders(task_name):
    input_folder = osp.join(osp.dirname(__file__), "../data/working")
    output_folder = osp.join(
        osp.dirname(__file__), "../data/output", task_name
    )
    logging_folder = osp.join(osp.dirname(__file__), "../logs", task_name)
    return input_folder, output_folder, logging_folder


def load_dataset(dataset_name, input_folder):
    """Load event list for a given dataset"""
    import joblib

    data = joblib.load("%s/%s/DATA.pkl" % (input_folder, dataset_name))
    return data


def get_hparam_str(args):
    return "kernel-scale={},base-constant={}".format(
        args.kernel_scale, args.base_constant
    )


def get_model_config(data, args):
    import numpy as np

    from model.base_intensity import ConstantBaseIntensity
    from model.kernels import ExponentialKernel
    from model.mark_density import TextualMarkDensity

    n_dimensions = len(data["dimen_code"])
    n_features = len(data["vocabulary"])
    config = {
        "n_dimensions": n_dimensions,
        "base_intensities": [
            ConstantBaseIntensity() for _ in range(n_dimensions)
        ],
        "kernels": [
            ExponentialKernel(scale=args.kernel_scale)
            for _ in range(n_dimensions)
        ],
        "mark_density": TextualMarkDensity(
            n_features,
            lengths=np.empty(n_dimensions),
            feature_probs=None,
            weight=None,
        ),
    }
    return config
