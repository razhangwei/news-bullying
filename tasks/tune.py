import sys
import argparse
import os.path as osp

sys.path.append(osp.join(osp.dirname(__file__), "..", "lib"))

from model.source_identify_model import SourceIdentifyModel
from model.utils.log_utils import init_logging, get_logger
from utils import load_dataset, get_hparam_str, get_model_config, get_folders

TASK_NAME = "ParamTuning"
INPUT_FOLDER, OUTPUT_FOLDER, LOGGING_FOLDER = get_folders(TASK_NAME)


def split_events(events, split_ratio, by="number"):

    assert by in ["number", "time"], "by must be in [number, time]."

    if by == "number":
        n = len(events)
        idx1 = int(round(n * split_ratio[0]))
        idx2 = int(round(n * (split_ratio[0] + split_ratio[1])))
        train_events = events[:idx1]
        valid_events = events[idx1:idx2]
        test_events = events[idx2:]
    elif by == "time":
        max_time = events[-1][0]
        train_events = []
        i = 0
        while i < len(events):
            if events[i][0] < max_time * split_ratio[0]:
                train_events.append(events[i])
                i += 1
            else:
                break

        valid_events = []
        while i < len(events):
            if events[i][0] < max_time * (split_ratio[0] + split_ratio[1]):
                valid_events.append(events[i])
                i += 1
            else:
                break

        test_events = events[i:]

    return train_events, valid_events, test_events


def build_args():
    parser = argparse.ArgumentParser(
        description="Tuning hyperparameter on real data."
    )
    parser.add_argument(
        "--dataset",
        type=str,
        help="Dataset. Can only be in [ICWSM11-bully-1week, "
        "news_blog_top10, reddit-0.25K, reddit-1K]",
    )

    parser.add_argument(
        "--split_by", type=str, default="number", help="[time, number]"
    )
    parser.add_argument(
        "--split_ratio", type=float, nargs=3, default=[0.8, 0.1, 0.1]
    )
    parser.add_argument("--kernel_scale", type=float, default=300)
    parser.add_argument("--base_constant", type=float, default=1.0)

    parser.add_argument(
        "--email", action="store_true", help="Send email to notify. "
    )
    parser.add_argument(
        "--force",
        action="store_true",
        help="Force to run the experiments no matter if "
        "results have already cached.",
    )

    args = parser.parse_args()
    assert args.dataset is not None
    assert sum(args.split_ratio) == 1 and all(
        x >= 0 for x in args.split_ratio
    ), "split_ratio should be a list of positives and summed up to be 1."

    return args


def main(args):
    """tune the parameter on train and test"""

    init_logging(osp.join(LOGGING_FOLDER, args.dataset))
    logger = get_logger(__name__)
    data = load_dataset(args.dataset, INPUT_FOLDER)
    train_events, valid_events, test_events = split_events(
        data["events"], args.split_ratio, by=args.split_by
    )

    logger.info("args: %s" % args)

    path = "{}/{}/model_{}.pkl".format(
        OUTPUT_FOLDER, args.dataset, get_hparam_str(args)
    )

    try:
        if not args.force and osp.exists(path):
            logger.info("Found trained file. ")
            model = SourceIdentifyModel.load(path)
        else:
            logger.info("Training ...")
            model = SourceIdentifyModel(**get_model_config(data, args)).fit(
                train_events, verbose=True
            )
            model.save(path)

            logger.info("Finished.")

        l1 = model.eval_log_likelihood(train_events)
        l2 = model.eval_log_likelihood(train_events + valid_events)
        logger.info(
            "ll_train: %f; ll_train/validation: %f; "
            "cond_ll: %f" % (l1, l2, l2 - l1)
        )

    except Exception:
        logger.error("Error occurred", exc_info=True)


if __name__ == "__main__":

    args = build_args()
    main(args)
