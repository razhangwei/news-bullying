import os
import sys

import joblib
import numpy as np
import scipy as sp

TASK_NAME = "TrainOnFullData"


def parse_args():

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", type=str)
    parser.add_argument("--n_dimensions", type=int, default=10)
    parser.add_argument("--method", type=str)
    parser.add_argument("--ratio", type=float)

    args = parser.parse_args()
    assert args.dataset is not None
    assert args.method is not None
    assert not (args.method == "NB" and args.ratio is None)

    return args


def load_dataset(dataset):
    data_path = os.path.join(
        os.path.dirname(__file__), "../data/working", dataset, "DATA.pkl"
    )
    data = joblib.load(data_path)

    events = data["events"]
    roots = data["roots"]

    return events, roots


def train_naive_bayes(args):
    from sklearn.naive_bayes import MultinomialNB
    from sklearn.model_selection import train_test_split

    events, roots = load_dataset(args.dataset)
    X = sp.sparse.vstack([e[2] for e in events])
    y = np.array(roots)
    train_index, test_index = train_test_split(
        range(X.shape[0]), stratify=y, train_size=args.ratio
    )

    def onehot(y, n_classes):
        b = np.zeros((len(y), n_classes))
        b[np.arange(b.shape[0]), y] = 1
        return b

    # print set(y[train_index])
    model = MultinomialNB().fit(X[train_index], y[train_index])
    y_pred = np.full((len(events), args.n_dimensions), np.nan)
    y_pred[test_index] = onehot(
        model.predict(X[test_index]), args.n_dimensions
    )

    rooted_proba = y_pred

    filename = "rp_ratio={}.pkl".format(args.ratio)
    rp_path = os.path.join(
        os.path.dirname(__file__),
        "../data/otuput",
        TASK_NAME,
        args.dataset,
        args.method,
        filename,
    )
    if not os.path.exists(os.path.dirname(rp_path)):
        os.makedirs(os.path.dirname(rp_path))
    joblib.dump(rooted_proba, rp_path)


if __name__ == "__main__":
    args = parse_args()

    if args.method == "NB":
        train_naive_bayes(args)
