from model.source_identify_model import SourceIdentifyModel

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import joblib

# configuaration
TASK_NAME = "RealExperiments"
INPUT_FOLDER = "../data/working"
OUTPUT_FOLDER = "../data/output/%s" % TASK_NAME
FIGURE_FOLDER = "../figures/%s" % TASK_NAME

df_news = pd.read_pickle("%s/MAINSTREAM_NEWS_bullying.pkl" % INPUT_FOLDER)
df_social = pd.read_pickle("%s/SOCIAL_MEDIA_bullying.pkl" % INPUT_FOLDER)
vectorizer = joblib.load("%s/vectorizer.pkl" % INPUT_FOLDER)

models = []
for scale1 in [60, 120, 300]:
    for scale2 in [1800, 3600, 9000]:
        model = SourceIdentifyModel.load(
            "%s/model_scale1=%d_scale2=%d.pkl"
            % (OUTPUT_FOLDER, scale1, scale2)
        )
        models.append(model)
        print(scale1, "\t", scale2)
        print()

np.set_printoptions(3)
for model in models:
    print((model.base_intensity_weights, "\n"))

for model in models:
    print((model.influential_matrix))

for model in models:
    print((model.mark_density.weight, "\n"))


def plot_sample_root_vs_predict_root(df):

    dim_names = ["tweet", "news"]
    n_dimensions = 2
    for root in range(n_dimensions):
        for i in range(n_dimensions):
            t = df.query("root == %d and dimension ==%d" % (root, i))
            plt.subplot(
                n_dimensions, n_dimensions, i * n_dimensions + root + 1
            )
            plt.scatter(t.timestamp, t["rp%d" % root], s=5)
            plt.xlim(xmin=0)
            plt.hlines(0.5, 0, plt.xlim()[1], linestyles="dashed")
            plt.ylim([0, 1])

            plt.ylabel("RP(%s)" % dim_names[root])
            plt.xlabel("time")
            plt.title("%s, root=%s" % (dim_names[i], dim_names[root]))

    plt.gcf().suptitle("")
    plt.tight_layout()
