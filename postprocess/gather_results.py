from os import path as osp
import sys

sys.path.append(osp.join(osp.dirname(__file__), "..", "lib"))
from copy import deepcopy
import argparse

import joblib
from model.source_identify_model import SourceIdentifyModel


def build_paser():

    parser = argparse.ArgumentParser(
        description="Running experiments on real dataset."
    )
    # add variable arguments
    parser.add_argument("task", type=str, help="Task.")
    parser.add_argument("dataset", type=str, help="Dataset.")

    parser.add_argument(
        "--kernel_scale",
        type=int,
        default=3600,
        help="Scale paramter shared by all exponential kernels."
        "Default is 3600",
    )
    parser.add_argument(
        "--base_constant",
        type=float,
        default=1.0,
        help="Constant value for ConstantBaseIntensity.",
    )

    return parser


def get_hparam_str(args):
    return "kernel-scale={}_base-constant={}".format(
        args.kernel_scale, args.base_constant
    )


if __name__ == "__main__":
    parser = build_paser()
    args = parser.parse_args()

    data_path = osp.join(
        osp.join(
            osp.dirname(__file__), "../data/working", args.dataset, "DATA.pkl"
        )
    )
    cache_dir = osp.join(
        osp.join(
            osp.dirname(__file__), "../data/output", args.task, args.dataset
        )
    )
    model_path = osp.join(
        cache_dir, "model_{}.pkl".format(get_hparam_str(args))
    )
    rp_path = model_path.replace("model", "rp")

    model = SourceIdentifyModel.load(model_path)
    rp = joblib.load(rp_path)
    data = joblib.load(data_path)

    # sanity check
    df = data["dataframe"]
    assert len(data["events"]) == len(df)

    summary = deepcopy(data)
    df["root_proba"] = [rp[i] for i in range(rp.shape[0])]
    summary["dataframe"] = df
    summary["influential_matrix"] = model.influential_matrix

    # export
    output_path = model_path.replace("model", "summary")
    joblib.dump(summary, output_path)
