"""
This file contains multiple classes for base intensity.
"""


from abc import ABCMeta, abstractmethod
import pickle

from scipy.optimize import minimize
from sklearn.base import BaseEstimator
import numpy as np
from numpy import sin, cos, sum, pi, min, array


class BaseIntensity(BaseEstimator, metaclass=ABCMeta):
    """
    Base class for base intensity.
    """

    @abstractmethod
    def eval(self, t):
        """ Evaluate mu(t) """
        pass

    @abstractmethod
    def eval_integral(self, t):
        """ Evaluate int_0^t mu(s) ds """
        pass

    @abstractmethod
    def eval_upperbound(self, t):
        """ Evaluate upper bound on [t, infty) """
        pass

    @abstractmethod
    def init_params(self, data, param_values=None):
        """ Initialize parameters using data"""
        pass

    @abstractmethod
    def save(self, to_file):
        """ Save model to file """
        pass


class ConstantBaseIntensity(BaseIntensity):
    """ Constant base intensity """

    def __init__(self, C=1):
        self.C = C

    def eval(self, t):
        return (array(t) >= 0).astype(float) * self.C

    def eval_integral(self, t):
        return (array(t) >= 0).astype(float) * array(t) * self.C

    def eval_upperbound(self, t):
        return (array(t) >= 0).astype(float) * self.C

    def init_params(self, data, param_values=None):
        pass

    def save(self, to_file):
        pickle.dump(self.C, to_file)

    @staticmethod
    def load(from_file):
        """ Load model from file """
        return ConstantBaseIntensity(C=pickle.load(from_file))


class SineBaseIntensity(BaseIntensity):
    r"""
    Sinine base intensity of the form
    .. math:: \mu(t) = \sum_i {A_i \sin(2 * \pi / T_i * t + \omege_i) } + A_0
    """

    def __init__(self, n_components, periods, amplitudes=None, shifts=None):

        self.n_components = n_components
        self.amplitudes = array(amplitudes)
        self.periods = array(periods)

        if shifts is None:
            shifts = np.zeros(n_components)
        self.shifts = array(shifts)

    def eval(self, t):
        t = array(t).reshape((-1, 1))
        res = self.amplitudes[0] + sum(
            self.amplitudes[1:] * sin(2 * pi / self.periods * t + self.shifts),
            axis=1,
        )
        return res[0] if len(res) == 1 else res

    def eval_integral(self, t):
        t = array(t).reshape((-1, 1))
        res = self.amplitudes[0] * t.ravel() - sum(
            self.amplitudes[1:]
            * (
                cos(self.shifts) - cos(2 * pi / self.periods * t + self.shifts)
            ),
            axis=1,
        )
        return res[0] if len(res) == 1 else res

    def eval_upperbound(self, t):
        return np.ones_like(t) * sum(self.amplitudes)

    def init_params(
        self, data, param_values=None, run_times=10, normalize=True
    ):
        """
        fit
        :param data:
        :param param_values:
        :param run_times:
        :return:
        """

        if param_values is None:
            param_values = {}
        if "amplitudes" in param_values:
            self.amplitudes = param_values["amplitudes"]
        if "periods" in param_values:
            self.periods = param_values["periods"]
        if "shifts" in param_values:
            self.shifts = param_values["shifts"]

        # compute the bin count
        bin_size = min(self.periods) / 24
        timestamps = array([e[0] for e in data])
        timestamps.sort()

        # assume the t[0] is zero or small
        bin_counts = []
        for t in timestamps:
            while t >= len(bin_counts) * bin_size:
                bin_counts.append(0)

            if (
                (len(bin_counts) - 1) * bin_size
                <= t
                < len(bin_counts) * bin_size
            ):
                bin_counts[-1] += 1
            else:
                raise ValueError()

        bin_locs = (0.5 + np.arange(len(bin_counts))) * bin_size

        def objective(x):
            """ square loss """
            A = x[: self.n_components + 1]
            w = x[self.n_components + 1 :]
            y = A[0] + (
                A[1:] * sin(2 * pi / self.periods * bin_locs[:, None] + w)
            ).sum(axis=1)
            return np.square(y - bin_counts).sum()

        best_fun = None
        bounds = [(0, None)] * (self.n_components + 1) + [
            (0, 2 * pi)
        ] * self.n_components
        for _ in range(run_times):
            # randomize starting point
            A0 = np.zeros(self.n_components + 1)
            A0[0] = np.mean(bin_counts)
            A0[1:] = np.random.exponential(scale=np.std(bin_counts))
            w0 = np.random.rand(self.n_components) * 2 * pi
            x0 = np.r_[A0, w0]

            res = minimize(objective, x0, method="TNC", bounds=bounds)

            if best_fun is None or res.fun < best_fun:
                best_fun = res.fun
                best_x = res.x

        self.amplitudes = best_x[: self.n_components + 1]
        self.shifts = best_x[self.n_components + 1 :]

        # normalize to integral over t equals t
        if normalize:
            tmax = np.max(timestamps) * 100
            integral = self.eval_integral(tmax)
            self.amplitudes *= tmax / integral

        return self

    def save(self, to_file):
        """
        save model to file
        @param to_file: pickle file
        """
        pickle.dump(self.n_components, to_file)
        pickle.dump(self.periods, to_file)
        pickle.dump(self.amplitudes, to_file)
        pickle.dump(self.shifts, to_file)

    @staticmethod
    def load(from_file):
        """
        load model from file
        @param from_file: pickle file
        """
        n_components = pickle.load(from_file)
        periods = pickle.load(from_file)
        amplitudes = pickle.load(from_file)
        shifts = pickle.load(from_file)

        return SineBaseIntensity(
            n_components, periods, amplitudes=amplitudes, shifts=shifts
        )
