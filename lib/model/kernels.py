# class of excitation kernels
"""
Excitation Kernels
"""


from abc import ABCMeta, abstractmethod
import pickle

from numpy import exp, log
from sklearn.base import BaseEstimator


class ExcitationKernel(BaseEstimator, metaclass=ABCMeta):
    """abstract class of excitation kernel. methods must be redefined in an
    inherited class."""

    @abstractmethod
    def eval(self, t):
        """ Evaluate the kernel value"""
        pass

    @abstractmethod
    def eval_integral(self, t):
        """ Evaluate the integral of kernel from 0 to (every element) of t"""
        pass

    @abstractmethod
    def eval_decay_factor(self, t):
        """ Evaluate the decay factor after passing time t"""
        pass

    @abstractmethod
    def eval_inverse_decay_factor(self, decay_factor):
        """ Evaluate the time such that after passing time t the kernel decays
        decay_factor"""
        pass


class ExponentialKernel(ExcitationKernel):
    """Kernel taking the form of 1 / scale x exp(-t / scale)"""

    def __init__(self, scale):
        assert scale > 0, "Time_decay parameter should be greater than 0."
        self.scale = float(scale)

    def eval(self, t):
        return 1 / self.scale * exp(-t / self.scale) if t >= 0 else 0

    def eval_integral(self, t):
        return 1 - exp(-t / self.scale) if t >= 0 else 0

    def eval_decay_factor(self, t):
        assert t >= 0, "t must be greater than or equal to 0."
        return exp(-t / self.scale)

    def eval_inverse_decay_factor(self, decay_factor):
        assert (
            0 < decay_factor < 1
        ), "decay factor must be strictly in between 0 and 1. "
        return -log(decay_factor) * self.scale

    def save(self, to_file):
        """ save the model """
        pickle.dump(self.scale, to_file)

    @staticmethod
    def load(from_file):
        """ load the model """
        scale = pickle.load(from_file)
        return ExponentialKernel(scale=scale)
