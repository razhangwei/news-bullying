"""
Mark density
"""

from abc import ABCMeta, abstractmethod

import pickle

import numpy as np
from numpy import random, array, log
from scipy import stats
from scipy.sparse.csr import csr_matrix
from scipy.special import gammaln
from sklearn.base import BaseEstimator

from .utils.utils import normalize


class MarkDensity(BaseEstimator, metaclass=ABCMeta):
    """
    Base class for mark density.
    """

    @abstractmethod
    def eval_log_proba_immigrant(self, x, ind):
        """
        Evaluate log[p(x|ind, theta)]
        :param x:
        :param ind:
        :return:
        """
        pass

    @abstractmethod
    def eval_log_proba_offspring(self, x, ind, e):
        """
        Evaluate log[p(x|ind, e, theta)]
        :param x:
        :param ind:
        :param e: parent event, a tuple of (ti, si, xi)
        :return:
        """
        pass

    @abstractmethod
    def draw_immigrant(self, ind):
        """
        Draw mark from p(x|ind, theta)
        :param ind:
        :return: sampled mark
        """
        pass

    @abstractmethod
    def draw_offspring(self, ind, e):
        """
        Draw mark from p(x|ind, e, theta)
        :param ind:
        :param e: parent event, a tuple of (ti, si, xi)
        :return: sampled mark
        """
        pass

    @abstractmethod
    def init_params(self, data, params_value=None):
        """
        Initialize the parameter before fitting
        :param data: list of events
        :params_value: dict
        :return:
        """
        pass

    @abstractmethod
    def fit(self, data, soft_parents):
        """
        Update theta given events and their parental relationships
        :param data: list of events
        :param soft_parents: parameters of proposed multinomial distribution,
            which can be viewed as soft parent
        :return:
        """
        pass


class SpatialMarkDensity(MarkDensity):
    r"""
    Spatial mark density with mark x_e are drawn by two cases:
    1. if e ~ /mu_i(t), then x_e ~ N(/nu_i, /Gamma_i)
    2. if e ~ /lambda_{e',i}(t), then x_e ~ N(x_e', /Sigma_{i_e'})
    """

    def __init__(
        self,
        n_features,
        immigrant_means,
        immigrant_covars,
        offspring_covars,
        covar_type="full",
        rand_state=None,
    ):
        """
        Constructor
        :param n_features: the mark dimensions
        :param immigrant_means: 2-D array of shape (n_dimension, n_features)
            the mean of the Gaussian for immigrant case.
        :param immigrant_covars: 3-D array of shape
            (n_dimension, n_features, n_features)
            the covariances of the Gaussian for immigrant case.
        :param offspring_covars: 3-D array of shape
            (n_dimension, n_features, n_features)
            the covariances of the Gaussian for offspring case.
        :param covar_type: string, could be either 'full' or 'diag'
            the type of covariance matrices
        :param rand_state: random generator
        """
        # TODO: check the validity of input arguments
        self.n_features = n_features
        self.immigrant_means = array(immigrant_means)
        self.immigrant_covars = array(immigrant_covars)
        self.offspring_covars = array(offspring_covars)
        self.rand_state = rand_state
        self.covar_type = covar_type
        self._check()

    def _check(self):
        assert (
            isinstance(self.n_features, (int, np.integer))
            and self.n_features > 0
        ), (
            "n_features must be a positive integer. Found: %r"
            % self.n_features
        )

        assert (
            self.covar_type == "full" or self.covar_type == "diag"
        ), "covar_type must be either 'full' or 'diag'. "

        self.immigrant_means = array(self.immigrant_means).reshape(
            (-1, self.n_features)
        )

        if self.covar_type == "full":
            self.immigrant_covars = array(self.immigrant_covars).reshape(
                (-1, self.n_features, self.n_features)
            )
            self.offspring_covars = array(self.offspring_covars).reshape(
                (-1, self.n_features, self.n_features)
            )
        elif self.covar_type == "diag":
            self.immigrant_covars = array(self.immigrant_covars).reshape(
                (-1, self.n_features)
            )
            self.offspring_covars = array(self.offspring_covars).reshape(
                (-1, self.n_features)
            )

        assert self.immigrant_means.shape[0] == self.immigrant_covars.shape[0]
        assert self.immigrant_means.shape[0] == self.offspring_covars.shape[0]

    def draw_offspring(self, ind, e):
        """
        Draw a mark from N(x_e, Sigma_{i_e}) for dimension ind
        :param ind: the dimension index
        :param e: tuple of length 3, as (ti, si, xi)
        :return: the sampled mark
        """
        _, si, xi = e
        return random.multivariate_normal(xi, self.offspring_covars[si])

    def draw_immigrant(self, ind):
        return random.multivariate_normal(
            self.immigrant_means[ind], self.immigrant_covars[ind]
        )

    def init_params(self, data, params_value=None):
        raise NotImplementedError()

    def fit(self, data, soft_parents, flag=0):
        """Fit parameters

        Parameters
        ----------
        data : list of (float, int, array_like)
            Events.

        soft_parents : csr_matrix
            Soft parents variables, i.e. the parameters for mean-field
            variational distribution.

        Returns
        -------
        self
        """

        if flag in [0, 1]:
            # update immigrant_means
            means = np.zeros_like(self.immigrant_means, dtype="float64")
            covars = np.zeros_like(self.immigrant_covars, dtype="float64")
            soft_counts = np.zeros(means.shape[0])
            for i, (_, si, xi) in enumerate(data):
                soft_counts[si] += soft_parents[i, 0]
                means[si] += soft_parents[i, 0] * xi

            self.immigrant_means = means / soft_counts[:, None]

            # update immigrant_covars
            for i, (_, si, xi) in enumerate(data):
                t = xi - self.immigrant_means[si]
                if self.covar_type == "full":
                    covars[si] += soft_parents[i, 0] * np.outer(t, t)
                elif self.covar_type == "diag":
                    covars[si] += soft_parents[i, 0] * (t * t)

            if self.covar_type == "full":
                self.immigrant_covars = covars / soft_counts[:, None, None]
            elif self.covar_type == "diag":
                self.immigrant_covars = covars / soft_counts[:, None]

        if flag in [0, 2]:
            # update offspring variance
            covars = np.zeros_like(self.offspring_covars, dtype="float64")
            soft_counts = np.zeros(self.offspring_covars.shape[0])

            for i, j in soft_parents[:, 1:].nonzero():

                _, si, xi = data[i]
                _, sj, xj = data[j]

                soft_counts[sj] += soft_parents[i, j + 1]
                diff = xi - xj
                if self.covar_type == "full":
                    covars[sj] += soft_parents[i, j + 1] * np.outer(diff, diff)
                elif self.covar_type == "diag":
                    covars[sj] += soft_parents[i, j + 1] * (diff * diff)

            if self.covar_type == "full":
                self.offspring_covars = covars / soft_counts[:, None, None]
            elif self.covar_type == "diag":
                self.offspring_covars = covars / soft_counts[:, None]

        return self

    def eval_log_proba_immigrant(self, x, ind):
        return log(
            self.eval_multivariate_normal_pdf(
                x, self.immigrant_means[ind], self.immigrant_covars[ind]
            )
        )

    def eval_log_proba_offspring(self, x, ind, e):
        _, si, xi = e
        return log(
            self.eval_multivariate_normal_pdf(x, xi, self.offspring_covars[si])
        )

    def eval_multivariate_normal_pdf(self, x, mu, covar):
        """ Evaluate the pdf for multivariate normal """
        # TODO: multivariate_normal.pdf is very slow

        if self.covar_type == "full":
            return stats.multivariate_normal.pdf(x, mu, covar)
        else:
            raise NotImplementedError()

    @staticmethod
    def load(from_file):
        """ Load model from pickle file """
        raise NotImplementedError()


class TextualMarkDensity(MarkDensity):
    """ Mark Density with Textual Mark

    Textual mark density with mark x_e are drawn by two cases:
    1. if e ~ mu_i(t), then
        l_e ~ Poisson(lambda_i),
        x_e ~ Multi(theta_i)
    2. if e ~ lambda_{e',i}(t), then
        l_e ~ Poisson(lambda_i)
        x_e ~ Multi( (1-gamma) theta_{i_e} + gamma * \tilde(x_e'} )


    Parameters
    ----------
    n_features : int
        The dimension of texts

    lengths : array_like, shape (-1,)
        The mean length of texts for each dimension.

    feature_probs : array_like, shape (n_features, -1)
        The multinomial parameters.

    weight : float, 0 < weight < 1, optional(default=0.3)
        Weight parameter in offspring, i.e. gamma in the above formulation

    Attributes
    ----------
    n_features : int
        The dimension of texts

    lengths : array_like, shape (-1,)
        The mean length of texts for each dimension.

    feature_probs : array_like, shape (n_features, -1)
        The multinomial parameters.

    weight : float, 0 < weight < 1, optional(default=0.3)
        Weight parameter in offspring, i.e. gamma in the above formulation

    """

    def __init__(self, n_features, lengths, feature_probs, weight=0.3):

        self.n_features = n_features
        self.lengths = array(lengths)
        self.feature_probs = array(feature_probs)
        self.weight = weight
        self._check()

    def _check(self):
        # TODO: add parameter checking
        pass

    def eval_log_proba_immigrant(self, x, ind):
        n = x.sum()
        x_valid = x.data
        pvals_valid = self.feature_probs[ind][x.indices]
        return (
            n * log(self.lengths[ind])
            - self.lengths[ind]
            + np.sum(x_valid * log(pvals_valid) - gammaln(x_valid + 1))
        )

    def eval_log_proba_offspring(self, x, ind, e):
        n = x.data.sum()
        _, _, xi = e

        x_valid = x.data
        pvals = (1 - self.weight) * self.feature_probs[ind]
        pvals[xi.indices] += (self.weight / xi.data.sum()) * xi.data
        pvals_valid = pvals[x.indices]
        return (
            n * log(self.lengths[ind])
            - self.lengths[ind]
            + np.sum(x_valid * log(pvals_valid) - gammaln(x_valid + 1))
        )

        # an optimized version, which unfortunately turns out to be slower than
        # the above:
        # ix, ix_e = union_list(x.indices, xi.indices)
        # x_valid = x.data
        # pvals_valid = (1 - self.weight) * self.feature_probs[ind, x.indices]
        # pvals_valid[ix] += (self.weight / xi.data.sum()) * xi.data[ix_e]
        # return (n * log(self.lengths[ind]) - self.lengths[ind] +
        #         np.sum(x_valid * log(pvals_valid) - gammaln(x_valid + 1)))

    def draw_immigrant(self, ind):
        n = random.poisson(self.lengths[ind])
        while n == 0:
            n = random.poisson(self.lengths[ind])
        return csr_matrix(random.multinomial(n, self.feature_probs[ind]))

    def draw_offspring(self, ind, e):
        n = random.poisson(self.lengths[ind])
        while n == 0:
            n = random.poisson(self.lengths[ind])
        _, _, x = e
        pvals = (1 - self.weight) * self.feature_probs[ind]
        pvals[x.indices] += (self.weight / x.data.sum()) * x.data
        return csr_matrix(random.multinomial(n, pvals))

    def init_params(self, data, alpha=1e-3, params_value=None):
        new_feature_probs = (
            np.ones((len(self.lengths), self.n_features)) * alpha
        )
        new_lengths = np.zeros(len(self.lengths))
        counts = np.zeros_like(new_lengths)
        for _, si, xi in data:
            new_feature_probs[si, xi.indices] += xi.data
            new_lengths[si] += xi.data.sum()
            counts[si] += 1

        self.feature_probs = (
            new_feature_probs / new_feature_probs.sum(axis=1)[:, None]
        )

        # Note that counts could be zero for some dimensions
        idx = counts != 0
        self.lengths[idx] = new_lengths[idx] / counts[idx]
        self.lengths[~idx] = new_lengths.sum() / counts.sum()
        self.weight = 0.5

    def fit(self, data, soft_parents, alpha=1e-3):
        """"Fit parameters

        Parameters
        ----------
        data : list of (float, int, csr_)
            Events.

        soft_parents : csr_matrix
            Soft parents variables, i.e. the parameters for mean-field
            variational distribution.

        alpha : float, optional(default=1e-3)
            Additive smoothing parameter.

        Returns
        -------
        self
        """

        # update lengths
        L = np.zeros(len(data))
        sum_length = np.zeros_like(self.lengths)
        counts = np.zeros_like(self.lengths)
        for i, (_, si, xi) in enumerate(data):
            counts[si] += 1
            L[i] = xi.data.sum()
            sum_length[si] += L[i]

        # Note that counts could be zero for some dimensions
        idx = counts != 0
        self.lengths[idx] = sum_length[idx] / counts[idx]
        self.lengths[~idx] = sum_length.sum() / counts.sum()

        sum_count = 0
        sum_count_mutual = 0
        new_feature_probs = np.ones_like(self.feature_probs) * alpha

        for i, j in array(soft_parents.nonzero()).T:

            _, si, xi = data[i]
            sp_ij = soft_parents[i, j]

            if j == 0:
                new_feature_probs[si, xi.indices] += sp_ij * xi.data
                continue

            # compute /xi^{(s_i)}_j
            _, sj, xj = data[j - 1]
            numerator = (self.weight / L[j - 1]) * xj.toarray()[0, xi.indices]
            denominator = (1 - self.weight) * self.feature_probs[si][
                xi.indices
            ] + numerator
            coeff = numerator / denominator

            new_feature_probs[sj, xi.indices] += sp_ij * (1 - coeff) * xi.data

            # accumulate sufficient statistics for updating weight(gamma)
            sum_count_mutual += sp_ij * np.sum(coeff * xi.data)
            sum_count += sp_ij * L[i]

        normalize(self.feature_probs, axis=1)
        self.weight = sum_count_mutual / sum_count

        return self

    def save(self, to_file):
        """ save the model """

        pickle.dump(self.n_features, to_file)
        pickle.dump(self.lengths, to_file)
        pickle.dump(self.feature_probs, to_file)
        pickle.dump(self.weight, to_file)

    @staticmethod
    def load(from_file):
        """ load the model from file """

        n_features = pickle.load(from_file)
        lengths = pickle.load(from_file)
        features = pickle.load(from_file)
        weight = pickle.load(from_file)

        return TextualMarkDensity(n_features, lengths, features, weight=weight)
