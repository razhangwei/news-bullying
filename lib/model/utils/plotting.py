"""
Created on Oct 26, 2015

@author: zhangwei
"""
import os
import matplotlib.pyplot as plt
import numpy as np


def save_figure(fig, path, dpi=100):
    """
    Save figure to path. If path does not exist, generate folders recursively.
    fig: figure handler
    :type fig: matplotlib.figure.Figure
    path: filename with folder
    :type string
    dpi:
    """
    my_dir = os.path.dirname(path)
    if not os.path.exists(my_dir):
        os.makedirs(my_dir)

    fig.savefig(path, dpi=dpi)


def plot_graph(weights, labels, ax=None):
    import networkx as nx

    G = nx.Graph()
    for i in range(weights.shape[0]):
        for j in range(weights.shape[1]):
            if weights[i][j] != 0:
                G.add_edge(labels[i], labels[j], weight=weights[i][j])

    e_pos = [(u, v) for (u, v, d) in G.edges(data=True) if d["weight"] > 0]
    e_pos_weight = np.array([G[u][v]["weight"] for (u, v) in e_pos])
    # e_neg = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] < 0]
    # e_neg_weight = np.array([G[u][v]['weight'] for (u, v) in e_neg])

    # normalize width

    max_value = e_pos_weight.max()
    e_pos_weight *= 10 / max_value
    # e_neg_weight *= 10 / max_value

    pos = nx.circular_layout(G)  # get layout
    nx.draw_networkx_nodes(G, pos, node_size=700, ax=ax)  # draw nodes
    # nx.draw_networkx_edges(G, pos)			# draw edges
    nx.draw_networkx_edges(G, pos, edgelist=e_pos, width=e_pos_weight, ax=ax)
    # nx.draw_networkx_edges(G, pos, edgelist=e_neg, width=e_neg_weight,
    #                        alpha=0.5, edge_color='b', style='dashed', ax=ax)
    nx.draw_networkx_labels(
        G, pos, font_size=15, font_family="sans-serif", ax=ax  # draw labels
    )


def plot_transitions(pairs):
    # pairs = [(1, 2, 10), (1, 3, 20), (2, 5, 10)]
    import matplotlib.pyplot as plt
    import seaborn.apionly as sns

    pairs = sorted(pairs, key=lambda x: (x[0], x[1]))

    plt.figure()
    colors = sns.color_palette("muted", len(pairs))
    tot = 0
    for i, (u, v, w) in enumerate(pairs):
        if u < v:
            plt.hlines(list(range(tot, tot + w + 1)), u, v, colors=colors[i])
        else:
            plt.vlines(u, tot, tot + w, colors=colors[i])

        tot += w
    plt.ylim(ymin=-0.5)


def plot_cv_curve(
    x,
    scores,
    show_error=True,
    allow_missing=False,
    xlabel="",
    ylabel="",
    title="",
    xscale="log",
    yscale=None,
    ax=None,
):
    """Plot cross-validation curve with respect to some parameters.
    Parameters
    ----------
    x : array-like, shape (n_params, )
        The values of parameter

    scores: array-like, shape (n_params, n_folds)
        Each row store the CV results for one parameter value. Note that it may contain np.nan

    """
    if ax is None:
        ax = plt.gca()

    # axes style
    ax.grid(True)

    # plot curve
    if allow_missing:
        y = np.nanmean(scores, axis=1)
        err = np.nanstd(scores, axis=1)
    else:
        idx = ~np.isnan(scores).any(axis=1)
        x = np.asarray(x)[idx]
        y = np.mean(scores[idx], axis=1)
        err = np.std(scores[idx], axis=1)

    # style
    fmt = "o-"

    # plot curve
    if show_error:
        ax.errorbar(x, y, err, fmt=fmt)
    else:
        ax.plot(x, y, fmt=fmt)

    # set labels and title
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)

    # set axis limit
    if xscale is None:
        ax.set_xlim(xmin=0, xmax=max(x))
    elif xscale == "log":
        ax.set_xlim(xmin=min(x) / 2, xmax=max(x) * 2)

    # set axis scale
    if xscale is not None:
        ax.set_xscale(xscale)
    if yscale is not None:
        ax.set_yscale(yscale)


def plot_heatmap(
    A,
    B=None,
    labels=None,
    title="",
    table_like=False,
    color_bar=False,
    ax=None,
):
    """Draw heatmap along with dots diagram for visualizing a weight matrix."""

    if ax is None:
        ax = plt.gca()

    # square shaped
    ax.set_aspect("equal", "box")

    # turn off the frame
    ax.set_frame_on(True)

    # want a more natural, table-like display
    if table_like:
        ax.invert_yaxis()
        ax.xaxis.tick_top()

    # put the major ticks at the middle of each cell
    ax.set_yticks(np.arange(A.shape[0]) + 0.5, minor=False)
    ax.set_xticks(np.arange(A.shape[1]) + 0.5, minor=False)

    # turn off all ticks
    ax.xaxis.set_tick_params(top=False, bottom=False)
    ax.yaxis.set_tick_params(left=False, right=False)

    # add labels
    if labels is not None:
        ax.set_xticklabels(labels, rotation=90)
        ax.set_yticklabels(labels)

    ax.set_title(title)

    # draw heatmap
    A_normed = (A - A.min()) / (A.max() - A.min())

    heatmap = ax.pcolor(A_normed, cmap=plt.cm.Greys)

    # add dots
    if B is not None:
        assert B.shape == A.shape
        for (x, y), w in np.ndenumerate(B.T):
            r = 0.35 * np.sqrt(w / B.max())
            circle = plt.Circle(
                (x + 0.5, y + 0.5), radius=r, color="darkgreen"
            )
            ax.add_artist(circle)

    # add colorbar
    if color_bar:
        ax.get_figure().colorbar(heatmap, ticks=[0, 1], orientation="vertical")


def plot_bar(
    x,
    hue,
    hue_labels=None,
    xlabel="",
    ylabel="",
    title="",
    ax=None,
    cmap_name="Accent",
):
    """Plot the proportion of hue for each value of x.

    Parameters
    ----------
    x : array-like
        Group id.
    hue : array-like
        Hue id
    """
    if ax is None:
        ax = plt.gca()

    n_group = len(set(x))
    n_hue = len(set(hue))
    hue2id = {h: i for i, h in enumerate(list(set(hue)))}

    data = np.zeros([n_group, n_hue])
    for i in range(len(x)):
        data[x[i], hue2id[hue[i]]] += 1

    data = data / data.sum(axis=1)[:, None]

    # set the style
    default_color_list = ["lightgreen", "dodgerblue", "orangered", "black"]
    if n_hue <= len(default_color_list):
        colors = default_color_list[:n_hue]
    else:
        colors = plt.cm.get_cmap(cmap_name)(np.linspace(0, 1, n_hue))
    width = 0.5 / n_hue

    left = np.arange(n_group)
    for i in range(n_hue):
        rects = ax.bar(left + width * i, data[:, i], width, color=colors[i])

    ax.set_ylabel(ylabel)
    ax.set_xlabel(xlabel)
    ax.set_xticks(left + n_hue * width / 2)
    ax.set_xticklabels(list(map(str, list(range(n_group)))))

    # add ticks
    ax.yaxis.set_tick_params(left=True, right=True)
    ax.xaxis.set_tick_params(top=True, bottom=True)

    ax.set_xlim(xmin=-1 + width * n_hue, xmax=n_group)

    # add legend
    if hue_labels is not None and len(hue_labels) >= n_hue:
        ax.legend(
            labels=hue_labels[:n_hue],
            loc="center left",
            bbox_to_anchor=(1, 0.5),
        )

    ax.set_title(title)


def plot_ternary(root_proba, indices, colors=None, markers=None, fontsize=20):
    """
    plot the ternary plot the
    Parameters
    ----------
    root_proba:
    indices:
    colors:
    markers:
    fontsize:

    Returns
    ------
    """

    root_proba = np.array(root_proba)
    indices = np.array(indices)

    assert (len(root_proba) == len(indices)) and (
        np.size(root_proba) == 3 * np.size(indices)
    )
    assert (min(np.reshape(root_proba, np.size(root_proba))) >= 0) and (
        max(np.reshape(root_proba, np.size(root_proba))) <= 1
    )

    if colors is None:
        colors = ["red", "green", "blue"]

    if markers is None:
        markers = ["s", "D", "o"]

    for ind in indices:
        if ind in [0, 1, 2]:
            pass
        else:
            raise IndexError("Index other than 0,1,2")

    import ternary

    fig, ax = ternary.figure(scale=1)
    fig.set_size_inches(10, 10)
    ax.boundary(linewidth=2.0)
    ax.gridlines(color="blue", multiple=0.05, linewidth=0.5)
    ax.bottom_axis_label("$\\mu_1$", fontsize=fontsize, color="brown")
    ax.right_axis_label("$\\mu_2$", fontsize=fontsize, color="brown")
    ax.left_axis_label("$\\mu_3$", fontsize=fontsize, color="brown")

    # plot the prediction boundary
    p = (1.0 / 3, 1.0 / 3, 1.0 / 3)
    p1 = (0, 0.5, 0.5)
    p2 = (0.5, 0, 0.5)
    p3 = (0.5, 0.5, 0)
    ax.line(p, p1, linestyle="--", color="brown", linewidth=3)
    ax.line(p, p2, linestyle="--", color="brown", linewidth=3)
    ax.line(p, p3, linestyle="--", color="brown", linewidth=3)

    # plot scatter plot of the points
    for i in [1, 2, 3]:
        points = root_proba[indices == i]
        if len(points) == 0:
            continue
        ax.scatter(
            points,
            linewidth=3.5,
            marker=markers[i - 1],
            color=colors[i - 1],
            label="dimension " + str(i),
        )

    ax.ticks(axis="lbr", multiple=0.1, linewidth=1)
    ax.legend()
    ax.clear_matplotlib_ticks()
    ax.show()

    return ax
