# -*- coding:utf-8 -*-
import os
import os.path as osp
import time
import logging

LOG_DEFAULT_FORMAT = "[ %(asctime)s][%(module)s.%(funcName)s] %(message)s"
LOG_DEFAULT_LEVEL = logging.INFO


def strftime(t=None):
    """Get string of current time"""
    return time.strftime("%Y%m%d-%H%M%S", time.localtime(t or time.time()))


def init_logging(logging_dir, level=None, log_format=None):
    if not osp.exists(logging_dir):
        os.makedirs(logging_dir)
    if log_format is None:
        log_format = LOG_DEFAULT_FORMAT

    global LOG_DEFAULT_LEVEL
    if isinstance(level, str):
        level = logging.getLevelName(level)
    elif level is None:
        level = LOG_DEFAULT_LEVEL

    LOG_DEFAULT_LEVEL = level

    logging.basicConfig(
        filename=osp.join(logging_dir, strftime() + ".log"),
        format=log_format,
        level=level,
    )

    import numpy as np

    np.set_printoptions(precision=3)


def get_logger(name, level=None, log_format=None, print_to_std=True):
    """
    Get the logger

    level: if None, then use default=INFO
    log_format: if None, use default format
    print_to_std: default=True
    """
    if level is None:
        level = LOG_DEFAULT_LEVEL
    elif isinstance(level, str):
        level = logging.getLevelName(level)

    if log_format is None:
        log_format = LOG_DEFAULT_FORMAT
    logger = logging.getLogger(name)
    logger.setLevel(level)

    if print_to_std:
        import sys

        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(level)
        handler.setFormatter(logging.Formatter(log_format))
        logger.addHandler(handler)

    return logger
