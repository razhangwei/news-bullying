from joblib import Parallel, delayed
import argparse
import paramiko
from sklearn.model_selection import ParameterGrid

hosts = [
    "quasar-20",
    "quasar-22",
    "quasar-27",
    "quasar-28",
    "quasar-30",
    "quasar-31",
    "quasar-32",
    "quasar-33",
    "quasar-34",
    "quasar-37",
    "quasar-38",
    "quasar-39",
    "quasar-40",
    "quasar-41",
    "quasar-42",
    "quasar-43",
    "quasar-44",
    "quasar-46",
    "quasar-47",
    "quasar-48",
    "quasar-50",
    "quasar-51",
    "quasar-52",
    "quasar-21",
]

hosts += ["nebula-{}".format(i) for i in [1, 2, 3, 4, 6, 7, 8, 9]]
# hosts = ['localhost']


def parse_commands():
    parser = argparse.ArgumentParser()

    # add variable arguments
    parser.add_argument("command", type=str, help="The command to run.")
    parser.add_argument("--use_python2", action="store_true", default=False)
    parser.add_argument("-N", metavar="n_hosts", type=int, default=len(hosts))
    parser.add_argument("-M", metavar="n_jobs", type=int, default=1)
    parser.add_argument(
        "--pw", metavar="password", type=str, default="hugh0709"
    )

    args, params = parser.parse_known_args()
    print(args, params)

    assert (
        len(params) % 2 == 0
    ), "Parameter and candidate values must be matched."
    assert all(
        arg[:2] == "--" and arg[2] != "-" for arg in params[::2]
    ), "Parameter must be started with two '-'"
    assert len(set(params[::2])) == len(
        params[::2]
    ), "Can't have duplicate parameters."

    param_grid = {}
    for i in range(0, len(params), 2):
        param_grid[params[i][2:]] = params[i + 1].split(",")

    cmds = []
    for param in ParameterGrid(param_grid):
        if args.use_python2:
            cmd = "source activate python2; " + args.command
        else:
            cmd = args.command

        for param_name, param_value in list(param.items()):
            cmd += " --{} {}".format(param_name, param_value)

        cmds.append(cmd)

    return args, cmds


def runCommand(host, cmd, pw):

    s = paramiko.SSHClient()
    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        s.connect(host, password=pw)
        # s.connect(host)
    except:
        print("***** failed to connect host ", host, " *****", sep="")
        return False
    print("Running on {}: {}".format(host, cmd))

    stdin, stdout, stderr = s.exec_command(cmd, get_pty=True)

    for line in stdout:
        print("[{}]: {}".format(host, line), end="")

    s.close()
    return True


if __name__ == "__main__":
    # generate parameter combination
    args, cmds = parse_commands()
    n_jobs = int(args.N * args.M)
    outputs = Parallel(n_jobs=n_jobs)(
        delayed(runCommand)(hosts[i % len(hosts)], cmd, args.pw)
        for i, cmd in enumerate(cmds)
    )
