import numpy as np
from scipy.misc import logsumexp
from scipy.sparse import csr_matrix, csc_matrix
import requests


def normalize(A, axis=1):
    """
    Normalize A
    Parameters
    ----------
    A : numpy array or csr_matrix, shape (n, m)
        The matrix to be normalize

    axis : int
        The axis to be normalize.
    """

    if isinstance(A, np.ndarray):
        axis_sums = A.sum(axis=axis)
        A = A / np.expand_dims(axis_sums, axis=axis)

    elif isinstance(A, csr_matrix):
        if axis != 1:
            raise ValueError("axis must be 1 for csr_matrix.")

        row_sums = np.array(A.sum(axis=1))[:, 0]
        row_indices, _ = A.nonzero()
        A.data /= row_sums[row_indices]

    elif isinstance(A, csc_matrix):
        if axis != 0:
            raise ValueError("axis must be 0 for csc_matrix.")

        col_sums = np.array(A.sum(axis=0))[:, 0]
        _, col_indices = A.nonzero()
        A.data /= col_sums[col_indices]
    else:
        raise NotImplementedError("Not implemented for type=%s" % type(A))


def log_normalize(A, axis=1, ninf_as_zero=True):
    """Normalizes the input array so that the exponent of the sum is 1.

    Parameters
    ----------
    A : array
        Non-normalized input data.

    axis : int
        Dimension along which normalization is performed.

    Notes
    -----
    Modifies the input **inplace**.
    """
    if isinstance(A, np.ndarray):
        if ninf_as_zero:
            A[A == 0] = -np.inf
        A_lse = logsumexp(A, axis)
        A -= np.expand_dims(A_lse, axis=axis)
    elif isinstance(A, csr_matrix):
        if axis != 1:
            raise ValueError("axis must be 1 for csr_matrix.")

        for i in range(A.shape[0]):
            row_lse = logsumexp(A.data[A.indptr[i] : A.indptr[i + 1]])
            A.data[A.indptr[i] : A.indptr[i + 1]] -= row_lse
    else:
        raise NotImplementedError("Not implemented for type=%s" % type(A))


def send_email(subject, text="", recipients=None):
    """ Send email usiing Mailgun API
    """
    api_key = "key-4046c8e8c3b8a084c9af58a4c6d646a9"
    domain_name = "sandboxb4988c600059446fa83a419aa86571e8.mailgun.org"

    if recipients is None:
        recipients = ["zhangwei@cs.wisc.edu"]

    return requests.post(
        "https://api.mailgun.net/v3/%s/messages" % domain_name,
        auth=("api", api_key),
        data={
            "from": "Experiment Notifications <mailgun@%s>" % domain_name,
            "to": recipients,
            "subject": subject,
            "text": text,
        },
    )


def initialize_directories(paths):
    """ Create directories if not existed."""
    import os

    for path in paths:
        if not os.path.exists(path):
            os.makedirs(path)


def get_basename(path, with_ext=False):
    from os.path import basename, splitext

    if with_ext:
        return basename(path)
    else:
        return splitext(basename(path))[0]
