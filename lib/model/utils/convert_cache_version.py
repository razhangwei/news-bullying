from ..source_identify_model import SourceIdentifyModel

import argparse


def load_model(path, version):
    pass


def upgrade_model(model):
    """Upgrade model to next version"""
    pass


def save_model(path):
    pass


def main(args):
    pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Convert model cached files to higher version."
    )
    parser.add_argument("path", type=str, help="Path to the cached model.")
    parser.add_argument(
        "-V",
        "--version",
        default=None,
        help="Version Number. Default is the latest version.",
    )

    args = parser.parse_args()
    main(args)
