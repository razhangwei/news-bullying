"""
This file defines a list of impact functions for marks
"""


from abc import ABCMeta, abstractmethod

from sklearn.base import BaseEstimator


class ImpactFunction(BaseEstimator, metaclass=ABCMeta):
    """ Base class for impact function """

    @abstractmethod
    def eval(self, x):
        """ Evaluate $\beta(x)$ """
        pass

    @abstractmethod
    def fit(self, data, weights):
        """ update the parameter"""
        pass


class ConstantImpactFunction(ImpactFunction):
    """ Constant impact function """

    def eval(self, x):
        return 1

    def fit(self, data, weights):
        pass
