"""
This file implements a multivariate Hawkes process that support the computation
for rooted probability.
"""

import os
import pickle
import logging

import numpy as np
import copy as cp
from numpy import argmin, array, zeros, random, log, exp
from scipy.misc import logsumexp
from scipy.sparse import lil_matrix, csr_matrix
from sklearn.base import BaseEstimator
from tqdm import tqdm

from .base_intensity import (
    BaseIntensity,
    ConstantBaseIntensity,
    SineBaseIntensity,
)
from .impact_function import ConstantImpactFunction
from .kernels import ExponentialKernel
from .mark_density import TextualMarkDensity, SpatialMarkDensity
from .utils.utils import normalize, log_normalize
from .utils.log_utils import get_logger

logger = get_logger(__name__)


class SourceIdentifyModel(BaseEstimator):
    """Multivariate Hawkes processes

    Parameters
    ----------
    n_dimensions : int
        Number of dimensions.

    base_intensities : list[BaseIntensity]
        Base intensities for each dimensions.

    base_intensity_weights : array_like, shape (len(base_intensities), )
        Weights assigned to the base intensity of each dimension.

    influential_matrix : array_like, shape (n_dimensions, n_dimensions)
        Weights for self and mutual excitations.

    kernels: list[ExcitationKernel]
        Excitation kernels events at each dimension.

    mark_density : MarkDensity
        Mark density.

    impact_function : ImpactFunction, optional (default=ConstantImpactFunction())
        Impact function for event marks.

    cutoff : float, optional (default=1e-6)
        Cutoff for the decay of kernels. Use to determine the maximum look ahead
        window.

    Attributes
    ----------
    n_dimensions : int
        Number of dimensions.

    base_intensities : list[BaseIntensity]
        Base intensities for each dimensions.

    base_intensity_weights : array_like, shape (len(base_intensities), )
        Weights assigned to the base intensity of each dimension.

    influential_matrix : array_like, shape (n_dimensions, n_dimensions)
        Weights for self and mutual excitations.

    kernels: list[ExcitationKernel]
        Excitation kernels events at each dimension.

    mark_density : MarkDensity
        Mark density.

    impact_function : ImpactFunction, optional (default=ConstantImpactFunction())
        Impact function for event marks.

    cutoff : float
        Cutoff for the decay of kernels. Use to determine the maximum look ahead
        window.

    max_lookaheads_ : float
        Maximum look ahead window size.
    """

    VERSION = 1

    def __init__(
        self,
        n_dimensions,
        base_intensities,
        kernels,
        mark_density,
        base_intensity_weights=None,
        influential_matrix=None,
        impact_function=None,
        cutoff=1e-6,
    ):
        self.version = self.__class__.VERSION

        self.n_dimensions = n_dimensions
        self.base_intensities = base_intensities
        self.base_intensity_weights = base_intensity_weights
        if base_intensity_weights is not None:
            self.base_intensity_weights = np.atleast_1d(base_intensity_weights)

        self.kernels = kernels
        self.mark_density = mark_density
        self.influential_matrix = influential_matrix
        if influential_matrix is not None:
            self.influential_matrix = array(influential_matrix).reshape(
                (n_dimensions, n_dimensions)
            )

        if impact_function is not None:
            self.impact_function = impact_function
        else:
            self.impact_function = ConstantImpactFunction()

        self.cutoff = cutoff

        if 0 < cutoff < 1:
            self.max_lookaheads_ = [
                kernel.eval_inverse_decay_factor(cutoff) for kernel in kernels
            ]
        else:
            self.max_lookaheads_ = [np.inf] * n_dimensions

        self.max_lookaheads_ = array(self.max_lookaheads_)

    def _check(self):
        assert (
            isinstance(self.n_dimensions, (int, np.integer))
            and self.n_dimensions > 0
        ), (
            "n_dimensions must be a postive integer. Found: %r "
            % self.n_dimensions
        )
        for base_intensity in self.base_intensities:
            assert isinstance(base_intensity, BaseIntensity)
            # TODO: checking the remaining parameters

        assert self.influential_matrix is not None
        assert self.base_intensity_weights is not None

    def _simulate_candidate_events(self, curr_time, accumulative_excitation):
        r"""
        Simulate the next candidate events on each dimension

        Parameters
        ----------
        curr_time : float
            current timestamp, i.e. timestamp of the last event
        accumulative_excitation: array_like
            the accumulative excitation from each dimension without adjusting
            for the influential rate, i.e.
            .. math:: accum(s) = \sum_{i: t_i < t, s_i=s} \beta(x_i) \kappa^(s)(t-t_i)

        """
        tmax = np.inf  # the running minimum of next candidate event time
        candidate_next_event_times = []
        for s in range(self.n_dimensions):

            def upper_bound(t, s=s):
                """ upper bound of hazard rate for dimension s"""
                decay_factors = array(
                    [
                        kernel.eval_decay_factor(t - curr_time)
                        for kernel in self.kernels
                    ]
                )
                return self.base_intensity_weights[s] * self.base_intensities[
                    s
                ].eval_upperbound(t) + np.dot(
                    self.influential_matrix[s, :],
                    accumulative_excitation * decay_factors,
                )

            def hazard_rate(t, s=s):
                """ hazard rate for dimension s"""
                decay_factors = array(
                    [
                        kernel.eval_decay_factor(t - curr_time)
                        for kernel in self.kernels
                    ]
                )
                return self.base_intensity_weights[s] * self.base_intensities[
                    s
                ].eval(t) + np.dot(
                    self.influential_matrix[s, :],
                    accumulative_excitation * decay_factors,
                )

            next_event_time = curr_time
            while next_event_time < tmax:
                ub = upper_bound(next_event_time)
                next_event_time += random.exponential(1 / ub)

                if next_event_time <= tmax and random.random() * ub < hazard_rate(
                    next_event_time
                ):
                    break

            candidate_next_event_times.append(
                next_event_time if next_event_time <= tmax else np.inf
            )

            tmax = np.min([tmax, candidate_next_event_times[-1]])

        return candidate_next_event_times

    def simulate(self, n_max, verbose=False, rand_state=None):
        """
        Simulate the marked Hawkes process using Ogata's thinning procedure

        Parameters
        ----------
        n_max : float
            number of events to be simulated.
        verbose : boolean
            whether to print information
        rand_state:

        Returns
        -------
        events: list of (ti, si, xi)
            Simulated events
        parents: list of ze
            parent variables
        """
        self._check()

        for kernel in self.kernels:
            if not isinstance(kernel, ExponentialKernel):
                raise NotImplementedError("Only support ExponentialKernel.")

        self._check()
        curr_time = 0
        events = []
        events_index = [[] for _ in range(self.n_dimensions)]
        parents = []

        accumulative_excitation = zeros(self.n_dimensions)

        if rand_state is not None:
            np.random.seed(rand_state)

        for i in range(n_max):
            # draw the candidate time stamps
            candidate_next_event_times = self._simulate_candidate_events(
                curr_time, accumulative_excitation
            )

            ti = np.min(candidate_next_event_times)
            si = argmin(candidate_next_event_times)

            # draw the parent variable
            probs = [
                self.base_intensity_weights[si]
                * self.base_intensities[si].eval(ti)
            ]
            indices = [0]
            for s in range(self.n_dimensions):
                for j in reversed(events_index[s]):
                    tj, _, xj = events[j]
                    if ti - tj > self.max_lookaheads_[s]:
                        break
                    probs.append(
                        (
                            self.influential_matrix[si, s]
                            * self.impact_function.eval(xj)
                            * self.kernels[s].eval(ti - tj)
                        )
                    )
                    indices.append(j + 1)

            probs = np.array(probs) / sum(probs)
            zi = random.choice(indices, p=probs)

            # draw the mark
            if zi == 0:
                xi = self.mark_density.draw_immigrant(si)
            else:
                xi = self.mark_density.draw_offspring(si, events[zi - 1])

            events.append((ti, si, xi))
            parents.append(zi)
            events_index[si].append(i)

            # update accumuative exication
            accumulative_excitation *= array(
                [
                    kernel.eval_decay_factor(ti - curr_time)
                    for kernel in self.kernels
                ]
            )
            accumulative_excitation[si] += self.impact_function.eval(
                xi
            ) * self.kernels[si].eval(0)
            curr_time = ti

            if verbose:
                logger.info(
                    "event: {}; (ti, si): ({}, {}); "
                    "parent: {}".format(len(events) + 1, ti, si, zi)
                )

        return events, parents

    def init_params(self, data, params_value=None, rand_state=None):
        """Initialize parameters.

        Parameters
        ----------
        data : list of tuple (ti, si, xi)
            the events.

        params_value : dict
            possible values for initialization.

        rand_state:

        """

        np.random.seed(rand_state)

        if params_value is None or not isinstance(params_value, dict):
            params_value = {}

        # initialize parameter related to base intensity
        events = [[] for _ in range(self.n_dimensions)]
        for ti, si, xi in data:
            events[si].append((ti, si, xi))

        if "base_intensity" in params_value:
            self.base_intensities = params_value["base_intensities"]
        else:
            for s in range(self.n_dimensions):
                self.base_intensities[s].init_params(events[s], params_value)

        # initialize the parameters related to mark density
        self.mark_density.init_params(data, params_value=params_value)

        # initialize the soft parents
        soft_parents = lil_matrix((len(data), len(data) + 1), dtype=float)
        events_index = [[] for _ in range(self.n_dimensions)]
        for i, ei in enumerate(data):
            soft_parents[i, 0] = 1
            for s in range(self.n_dimensions):
                for j in reversed(events_index[s]):
                    if ei[0] - data[j][0] > self.max_lookaheads_[s]:
                        break

                    soft_parents[i, j + 1] = 1

            events_index[data[i][1]].append(i)

        if len(data) < 20000:
            self._soft_parents = soft_parents.toarray()
        else:
            self._soft_parents = soft_parents.tocsr()

        normalize(self._soft_parents, axis=1)

        self.influential_matrix = np.empty(
            (self.n_dimensions, self.n_dimensions)
        )

    def fit(
        self,
        data,
        c_proportion=0.5,
        n_iter=100,
        tol=1e-4,
        emperical_bayes=True,
        init_params=None,
        verbose=True,
        rand_state=None,
    ):
        """Fit Hawkes process using varitional EM

        Parameters
        ----------
        data : list of (ti, si, xi)
            Events for training.

        n_iter : int, optional(default=100)
            Maximum number of iterations.

        tol : float, optional(default=1e-4)
            Tolerance to determine the convergence.

        init_params : dict, optional(default=None)
            Provided values for some parameters.

        emperical_bayes: boolean, optional(default=True)
            Use emperical Bayes in fitting.

        verbose : boolean, optional(default=False)
            Whether to print information

        rand_state :  {None, int}, optional(default=None)
            Random seed.

        Returns
        -------
        self : object
            Returns self.
        """

        np.random.seed(rand_state)
        tmax = max(e[0] for e in data)

        # initialize parameters
        self.init_params(data, params_value=init_params)

        # initialize priors:
        if emperical_bayes:
            N_s = np.zeros(len(set([data_event[1] for data_event in data])))
            for data_event in data:
                N_s[data_event[1]] += 1
            a_param_rho = N_s  # c_proportion*N_s
            b_param_rho = tmax / c_proportion  # tmax
            # 180*N_s*(1-c_proportion)/250 #(N_s*(1-c_proportion)*180./(250.*tmax))**2.0 #N_s
            a_param_alpha = N_s / N_s
            # tmax #N_s*(1-c_proportion)*180./(250.*tmax)#tmax / (1-c_proportion) #tmax / c_proportion
            b_param_alpha = 1.0 / (
                (180.0 / 250.0) * (1.0 - c_proportion) * N_s / tmax
            )
            # b_param_alpha = 2 * len(N_s) * tmax
        else:
            a_param_rho = 1
            b_param_rho = 0
            a_param_alpha = 1
            b_param_alpha = 0

        # convergence check:
        convergence_check = 0

        # if logger.getEffectiveLevel() <= logging.DEBUG:
        #     logger.debug(self.eval_variational_lower_bound(data, tmax))

        objs = []
        for it in tqdm(
            list(range(n_iter)) if verbose else list(range(n_iter))
        ):

            # update the base intensity rate
            # ADD PRIOR IN LINE BELOW BY COMMENTING OUT FIRST LINE AND UNCOMMENTING SECOND: #
            # soft_base_count = np.full(self.n_dimensions, 1e-10)
            soft_base_count = (
                np.full(self.n_dimensions, 1e-10 - 1) + a_param_rho
            )
            for i, (_, si, _) in enumerate(data):
                soft_base_count[si] += self._soft_parents[i, 0]
            # ADD PRIOR BELOW BY COMMENTING OUT FIRST TWO LINES AND UNCOMMENTING SECOND TWO LINES: #
            # self.base_intensity_weights = soft_base_count \
            #    / array([base.eval_integral(tmax) for base in self.base_intensities])
            self.base_intensity_weights = soft_base_count / (
                b_param_rho
                + array(
                    [
                        base.eval_integral(tmax)
                        for base in self.base_intensities
                    ]
                )
            )

            # INCORPORATE PRIOR INTO THE EVALUATION OF THE VARIATIONAL LOWER BOUND BY ADDING N_s ARGUMENT:#
            if logger.getEffectiveLevel() <= logging.DEBUG:
                logger.debug(
                    "After update base rate: {}".format(
                        self.eval_variational_lower_bound(data, tmax, N_s)
                    )
                )

            # update the mutual excitation rate
            # ADD PRIOR IN LINE BELOW BY COMMENTING OUT FIRST LINE AND UNCOMMENTING SECOND: #
            # soft_base_count = np.full(self.n_dimensions, 1e-10)
            soft_base_count = np.full(self.n_dimensions, 1e-10) + b_param_alpha
            for ti, si, xi in data:
                soft_base_count[si] += self.impact_function.eval(
                    xi
                ) * self.kernels[si].eval_integral(tmax - ti)

            soft_mutual_count = np.zeros(
                (self.n_dimensions, self.n_dimensions)
            )
            # ADD PRIOR BELOW BY COMMENTING OUT FIRST LINE: #
            soft_mutual_count += a_param_alpha[:, np.newaxis] - 1
            for i, j in array(self._soft_parents[:, 1:].nonzero()).T:
                si = data[i][1]
                sj = data[j][1]
                soft_mutual_count[si, sj] += self._soft_parents[i, j + 1]
            self.influential_matrix = (soft_mutual_count.T / soft_base_count).T
            # print((self.influential_matrix[:, 0]))

            if logger.getEffectiveLevel() <= logging.DEBUG:
                logger.debug(
                    "After update influential matrix: {}".format(
                        self.eval_variational_lower_bound(data, tmax, N_s)
                    )
                )

            # XXX: update impact function; weight should compute the real weights
            # for noncontant impact function
            weights = None
            self.impact_function.fit(data, weights)

            # update the mark density
            self.mark_density.fit(data, self._soft_parents)
            if logger.getEffectiveLevel() <= logging.DEBUG:
                logger.debug(
                    "After update mark density: {}".format(
                        self.eval_variational_lower_bound(data, tmax, N_s)
                    )
                )

            # update the soft parent variable
            for i, j in array(self._soft_parents.nonzero()).T:

                ti, si, xi = data[i]
                if j == 0:
                    log_mark_prob = self.mark_density.eval_log_proba_immigrant(
                        xi, si
                    )
                    self._soft_parents[i, j] = (
                        log(
                            self.base_intensity_weights[si]
                            * self.base_intensities[si].eval(ti)
                        )
                        + log_mark_prob
                    )
                else:
                    tj, sj, xj = data[j - 1]
                    log_mark_prob = self.mark_density.eval_log_proba_offspring(
                        xi, si, data[j - 1]
                    )
                    self._soft_parents[i, j] = (
                        log(
                            self.influential_matrix[si, sj]
                            * self.impact_function.eval(xj)
                            * self.kernels[sj].eval(ti - tj)
                        )
                        + log_mark_prob
                    )

            # normalize the log representation and then change it to regular
            log_normalize(self._soft_parents, axis=1, ninf_as_zero=True)
            if isinstance(self._soft_parents, csr_matrix):
                self._soft_parents.data = exp(self._soft_parents.data)
            else:
                self._soft_parents = exp(self._soft_parents)

            if logger.getEffectiveLevel() <= logging.DEBUG:
                logger.debug(
                    "After update parents: {}".format(
                        self.eval_variational_lower_bound(data, tmax, N_s)
                    )
                )

            # judge the convergence
            objs.append(self.eval_variational_lower_bound(data, tmax, N_s))

            logger.info("iter=%d,\tobj=%g" % (it + 1, objs[-1]))
            logger.debug(self.influential_matrix)
            logger.debug(self.mark_density)

            if len(objs) > 1 and abs((objs[-1] - objs[-2]) / objs[-1]) < tol:
                convergence_check += 1
                if convergence_check > 1:
                    break

        return self

    def eval_rooted_proba(
        self, data, include_time=True, include_mark=True, verbose=False
    ):
        """Evaluate the rooted probability

        Parameters
        ----------
        data : list of tuple (ti, si, xi)
            Events.

        include_time : boolean, optional (default=True)
            Whether to include the effect of time.

        include_mark : boolean, optional (default=True)
            Wehther to include to effect to mark.

        verbose : boolean, optional (default=False)
            Whether to print out informations.

        Returns
        -------
        rooted_proba : array_like, shape (n_events, n_dimension)
            The rooted probability for each event for each dimension.
        """
        self._check()

        assert include_mark or include_time, (
            "include_time and include_mark can't both be false. "
            "Found(include_time={}, include_mark={}})".format(
                include_time, include_mark
            )
        )
        log_rooted_proba = np.zeros((len(data), self.n_dimensions))
        events_index = [[] for _ in range(self.n_dimensions)]

        for i, (ti, si, xi) in tqdm(
            enumerate(data) if verbose else enumerate(data)
        ):
            # logger.debug("\nevent: {}; dim: {}; timestamp: {}".format(i, si, ti))
            # log probability components
            log_prob_comps = [[-np.inf] for _ in range(self.n_dimensions)]

            # immigrant
            # from IPython.core.debugger import Tracer; Tracer()()
            temporal_comp = (
                log(
                    self.base_intensity_weights[si]
                    * self.base_intensities[si].eval(ti)
                )
                if include_time
                else 0
            )
            mark_comp = (
                self.mark_density.eval_log_proba_immigrant(xi, si)
                if include_mark
                else 0
            )
            log_prob_comps[si].append(temporal_comp + mark_comp)
            # logger.debug("base temporal: {:.3}; base mark: {:.3}".format(
            #    temporal_comp, mark_comp))

            for s in range(self.n_dimensions):
                for j in reversed(events_index[s]):
                    tj, _, xj = data[j]
                    if include_time and ti - tj > self.max_lookaheads_[s]:
                        break

                    # from IPython.core.debugger import Tracer; Tracer()()
                    # rootoed probability
                    log_prob_comp_j = log_rooted_proba[j].copy()
                    # logger.debug("Parent event: {}; dim: {}; timestamp: {}".format(
                    #    j, s, tj))
                    # logger.debug("log RP: {}".format(log_rooted_proba[j]))

                    if include_time:
                        log_prob_temp_comp_j = log(
                            self.influential_matrix[si][s]
                            * self.impact_function.eval(xj)
                            * self.kernels[s].eval(ti - tj)
                        )
                        log_prob_comp_j += log_prob_temp_comp_j
                        # logger.debug("temporal contrib: {:.3}".format(log_prob_temp_comp_j))

                    if include_mark:
                        log_prob_mark_comj_j = self.mark_density.eval_log_proba_offspring(
                            xi, si, data[j]
                        )
                        log_prob_comp_j += log_prob_mark_comj_j
                    #    logger.debug("mark contrib: {:.3}".format(log_prob_mark_comj_j))

                    for ss in range(self.n_dimensions):
                        log_prob_comps[ss].append(log_prob_comp_j[ss])

            log_rooted_proba[i] = list(map(logsumexp, log_prob_comps))
            log_rooted_proba[i] -= logsumexp(log_rooted_proba[i])

            # logger.debug("rooted proba: {}".format(log_rooted_proba[i]))

            events_index[si].append(i)

        return exp(log_rooted_proba)

    def eval_log_likelihood(self, data, tmax=None):
        """Evaluate the log likelihood
        """

        log_ll = 0
        if tmax is None:
            tmax = max(e[0] for e in data)

        for s in range(self.n_dimensions):
            log_ll -= self.base_intensity_weights[s] * self.base_intensities[
                s
            ].eval_integral(tmax)

        for i, (ti, si, xi) in enumerate(data[:-1]):
            temp = self.impact_function.eval(xi) * self.kernels[
                si
            ].eval_integral(tmax - ti)
            log_ll -= np.sum(self.influential_matrix[:, si] * temp)

        events_index = [[] for _ in range(self.n_dimensions)]
        for i, (ti, si, xi) in enumerate(data):

            temp = [
                log(
                    self.base_intensity_weights[si]
                    * self.base_intensities[si].eval(ti)
                )
                + self.mark_density.eval_log_proba_immigrant(xi, si)
            ]

            for s in range(self.n_dimensions):
                for j in reversed(events_index[s]):
                    tj, sj, xj = data[j]
                    if ti - tj > self.max_lookaheads_[s]:
                        break

                    temp.append(
                        log(
                            self.influential_matrix[si, sj]
                            * self.impact_function.eval(xj)
                            * self.kernels[sj].eval(ti - tj)
                        )
                        + self.mark_density.eval_log_proba_offspring(
                            xi, si, data[j]
                        )
                    )

            log_ll += logsumexp(temp)
            events_index[si].append(i)

        return log_ll

    def eval_intensity_integral(self, data):
        """Evaluate the intensity integral.
        """
        raise NotImplementedError()

    def eval_conditional_intensity(self, timestamps, data):
        """ Evaluate the conditional indensity

        Parameters
        ----------
        timestamps : array_like(float)
            Timestamps at which conditional intensity is evaluated.

        data : list of 3-tuples
            Historical events.

        Returns
        -------
        cond_intensities: array_like(float), shape (n_dimension, len(timestamps))
            Conditional intensities evaluated at specified timestamps for each
            dimension.
        """
        self._check()

        cond_intensities = np.zeros((self.n_dimensions, len(timestamps)))
        events_index = [[] for _ in range(self.n_dimensions)]
        max_index = 0
        for i, t in enumerate(timestamps):
            # add new events whose t_i is earlier than t.
            while max_index < len(data) and data[max_index][0] < t:
                events_index[data[max_index][1]].append(max_index)
                max_index += 1

            # determined the approximate range
            for s in range(self.n_dimensions):
                cond_intensities[s, i] = self.base_intensity_weights[
                    s
                ] * self.base_intensities[s].eval(t)
                for j in reversed(events_index[s]):
                    tj, sj, xj = data[j]
                    if t - tj > self.max_lookaheads_[s]:
                        break
                    cond_intensities[s, i] += (
                        self.influential_matrix[s, sj]
                        * self.impact_function.eval(xj)
                        * self.kernels[sj].eval(t - tj)
                    )

        return cond_intensities

    def cumulative_transform(self, data):
        r"""Compute integeral of conditional intensities.

        Transform each (ti, si, xi) to integral of it conditional intensity
        at its dimension si.
            .. math:: \Lambda^{s_i}(t_i | H_{t_i})

        Parameters
        ----------
        data : list of 3-tuples
            Historical events.

        Returns
        -------
        integrals: array_like(float), shape (len(data), )
            Conditional intensity integrals evaluated at specified timestamps
            for each dimension.
        """

        # sum_{i: t_i < t, s_i = s} \beta(x_i)
        integrals = zeros(len(data))
        sum_impact = zeros(self.n_dimensions)
        events_index = [[] for _ in range(self.n_dimensions)]
        for i, (ti, si, xi) in enumerate(data):
            diff_impact = zeros(self.n_dimensions)

            # compute the difference impact for \beta(x_j) and K(t_j, t_i) *
            # \beta(x_j)
            for s in range(self.n_dimensions):
                for j in reversed(events_index[s]):
                    tj, _, xj = data[j]
                    if ti - tj > self.max_lookaheads_[s]:
                        break

                    diff_impact[s] += (
                        1 - self.kernels[s].eval_integral(ti - tj)
                    ) * self.impact_function.eval(xj)

            integrals[i] = self.influential_matrix[si].dot(
                sum_impact - diff_impact
            )
            sum_impact[si] += self.impact_function.eval(xi)
            events_index[si].append(i)

        return integrals

    def eval_variational_lower_bound(self, data, tmax, N_s):
        """
        Evaluate the variational lower bound.

        Parameters
        ----------
        data : list of tuple (ti, si, xi)
            events

        tmax : float
            the maximum observation window.

        Returns
        -------
        bound: float
            lower bound value

        """

        # NEXT LINE RELEVANT TO INCLUDING PRIOR IN VARIATIONAL LOWER BOUND #
        num_data_events = N_s.sum()

        part1 = -sum(
            self.base_intensity_weights[s]
            * self.base_intensities[s].eval_integral(tmax)
            for s in range(self.n_dimensions)
        )

        part2, part3, part4 = zeros(3)

        total_influence = np.sum(self.influential_matrix, axis=0)

        for i, j in array(self._soft_parents.nonzero()).T:

            ti, si, xi = data[i]

            if j == 0:
                part2 += (
                    -self.impact_function.eval(xi)
                    * self.kernels[si].eval_integral(tmax - ti)
                    * total_influence[si]
                )

                part3 += self._soft_parents[i, 0] * (
                    log(self.base_intensity_weights[si])
                    + log(self.base_intensities[si].eval(ti))
                    + self.mark_density.eval_log_proba_immigrant(xi, si)
                    - log(self._soft_parents[i, 0])
                )
            else:
                tj, sj, _ = data[j - 1]

                mark_density_prob = self.mark_density.eval_log_proba_offspring(
                    xi, si, data[j - 1]
                )
                part4 += self._soft_parents[i, j] * (
                    log(self.influential_matrix[si, sj])
                    + log(self.kernels[sj].eval(ti - tj))
                    + mark_density_prob
                    - log(self._soft_parents[i, j])
                )

            logger.debug(
                "{}\t{}\t{}\t{}\t".format(
                    part1 + part3, part2 + part4, part3 + part4, part3 + part4
                )
            )

        # ADD PARTS 5 AND 6 TO ACCOUNT FOR PRIORS ON RHO AND ALPHA #
        part5 = sum(
            (
                (N_s[s] - 1) * log(self.base_intensity_weights[s])
                - 2 * tmax * self.base_intensity_weights[s]
            )
            for s in range(self.n_dimensions)
        )

        part6_s = np.zeros(self.n_dimensions)
        for s in range(self.n_dimensions):
            influential = cp.deepcopy(self.influential_matrix[s, :])
            influential_fixed = cp.deepcopy(influential)
            influential_fixed[influential_fixed < exp(-10)] = exp(-10)
            log_influential_s_s_prime = np.where(
                influential >= exp(-10), log(influential_fixed), -10
            )
            part6_s[s] = sum(
                (
                    (N_s[s] - 1) * log_influential_s_s_prime[s_prime]
                    - num_data_events
                    * tmax
                    * self.influential_matrix[s, s_prime]
                )
                for s_prime in range(self.n_dimensions)
            )
        part6 = part6_s.sum()

        # PARTS 5 AND 6 ADDED TO INCLUDE PRIOR INFO #
        lb = part1 + part2 + part3 + part4 + part5 + part6

        return lb

    @staticmethod
    def eval_roots(indices, parents):
        """

        :param indices: the dimension labels of each events
        :param parents: the parent relationship of each events
        :return: an array indicating the root of each events
        """
        roots = []
        for si, zi in zip(indices, parents):
            if zi == 0:
                roots.append(si)
            else:
                roots.append(roots[zi - 1])

        return roots

    def save(self, path):
        """ save the model
        @param path: file name
        """
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))

        with open(path, "wb") as fout:
            # Save the version id first; critical!
            pickle.dump(self.version, fout)

            pickle.dump(self.n_dimensions, fout)

            for intensity in self.base_intensities:
                pickle.dump(intensity.__class__.__name__, fout)
                intensity.save(fout)
            pickle.dump(self.base_intensity_weights, fout)

            for kernel in self.kernels:
                pickle.dump(kernel.__class__.__name__, fout)
                kernel.save(fout)

            pickle.dump(self.mark_density.__class__.__name__, fout)
            self.mark_density.save(fout)

            pickle.dump(self.influential_matrix, fout)
            pickle.dump(self.impact_function, fout)
            pickle.dump(self.cutoff, fout)

    @classmethod
    def load(cls, path):
        """
        load the model from file
        @param path: file name
        """
        with open(path, "rb") as fin:
            # check the version; important
            version = pickle.load(fin)
            if version != cls.VERSION:
                raise EnvironmentError(
                    "version {} of cached model does not "
                    "match to current version {}".format(version, cls.VERSION)
                )

            n_dimensions = pickle.load(fin)

            base_intensities = []
            for _ in range(n_dimensions):
                intensity_type = pickle.load(fin)
                if intensity_type == "ConstantBaseIntensity":
                    base_intensities.append(ConstantBaseIntensity.load(fin))
                elif intensity_type == "SineBaseIntensity":
                    base_intensities.append(SineBaseIntensity.load(fin))
                else:
                    raise NotImplementedError(
                        "BaseIntensity: " + intensity_type
                    )
            base_intensity_weights = pickle.load(fin)

            kernels = []
            for _ in range(n_dimensions):
                kernel_type = pickle.load(fin)
                if kernel_type == "ExponentialKernel":
                    kernels.append(ExponentialKernel.load(fin))
                else:
                    raise NotImplementedError(
                        "ExcitationKernel: " + kernel_type
                    )

            mark_density_type = pickle.load(fin)
            if mark_density_type == "TextualMarkDensity":
                mark_density = TextualMarkDensity.load(fin)
            elif mark_density_type == "SpatialMarkDensity":
                mark_density = SpatialMarkDensity.load(fin)
            else:
                raise NotImplementedError("MarkDensity: " + mark_density_type)

            influential_matrix = pickle.load(fin)
            impact_function = pickle.load(fin)

            cutoff = pickle.load(fin)

            return SourceIdentifyModel(
                n_dimensions=n_dimensions,
                base_intensities=base_intensities,
                base_intensity_weights=base_intensity_weights,
                influential_matrix=influential_matrix,
                kernels=kernels,
                mark_density=mark_density,
                impact_function=impact_function,
                cutoff=cutoff,
            )
