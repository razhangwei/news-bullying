# Source Identifying Bullying Process

Project for using Hawkes process to identify the causes for social media events.

## How to run it

1. Install Anaconda.
2. Install environment and packages in Anaconda. Please run the following command in the terminal:
` conda create -n hawkes -y python=2.7 numpy scipy scikit-learn matplotlib joblib requests tqdm pandas `
3. Activate Anaconda environment: `source activate hawkes`
4. Run "python tasks/train.py --force --dataset reddit-0.25K --kernel-scale 60"
